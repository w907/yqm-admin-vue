/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100424
 Source Host           : localhost:3306
 Source Schema         : yqm-admin-vue

 Target Server Type    : MySQL
 Target Server Version : 100424
 File Encoding         : 65001

 Date: 20/08/2023 22:38:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` bigint NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------

-- ----------------------------
-- Table structure for yqm_sys_config
-- ----------------------------
DROP TABLE IF EXISTS `yqm_sys_config`;
CREATE TABLE `yqm_sys_config`  (
  `config_id` int NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yqm_sys_config
-- ----------------------------
INSERT INTO `yqm_sys_config` VALUES (1, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2023-08-04 23:49:54', '1', '2023-08-20 12:40:43', '初始化密码 123456', '0');

-- ----------------------------
-- Table structure for yqm_sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `yqm_sys_dept`;
CREATE TABLE `yqm_sys_dept`  (
  `dept_id` bigint NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `not_operation` int NULL DEFAULT 1 COMMENT '是否允许操作(0: 不允许 1: 允许 )',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 202 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yqm_sys_dept
-- ----------------------------
INSERT INTO `yqm_sys_dept` VALUES (100, 0, '0', '亦秋美科技', 0, '亦秋美', '15888888888', 'yqm@qq.com', '0', '0', 'admin', '2023-08-04 23:49:54', '', '2023-08-04 23:49:54', 0);
INSERT INTO `yqm_sys_dept` VALUES (101, 100, '0,100', '深圳总公司', 1, '亦秋美', '15888888888', 'yqm@qq.com', '0', '0', 'admin', '2023-08-04 23:49:54', '', '2023-08-04 23:49:54', 1);
INSERT INTO `yqm_sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '亦秋美', '15888888888', 'yqm@qq.com', '0', '0', 'admin', '2023-08-04 23:49:54', '', '2023-08-04 23:49:54', 1);
INSERT INTO `yqm_sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '亦秋美', '15888888888', 'yqm@qq.com', '0', '0', 'admin', '2023-08-04 23:49:54', '', '2023-08-04 23:49:54', 1);
INSERT INTO `yqm_sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '亦秋美', '15888888888', 'yqm@qq.com', '0', '0', 'admin', '2023-08-04 23:49:54', '', '2023-08-04 23:49:54', 1);
INSERT INTO `yqm_sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '亦秋美', '15888888888', 'yqm@qq.com', '0', '0', 'admin', '2023-08-04 23:49:54', '', '2023-08-04 23:49:54', 1);
INSERT INTO `yqm_sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '亦秋美', '15888888888', 'yqm@qq.com', '0', '0', 'admin', '2023-08-04 23:49:54', '', '2023-08-04 23:49:54', 1);
INSERT INTO `yqm_sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '亦秋美', '15888888888', 'yqm@qq.com', '0', '0', 'admin', '2023-08-04 23:49:54', '', '2023-08-04 23:49:54', 1);
INSERT INTO `yqm_sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '亦秋美', '15888888888', 'yqm@qq.com', '0', '0', 'admin', '2023-08-04 23:49:54', '', '2023-08-04 23:49:54', 1);
INSERT INTO `yqm_sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '亦秋美', '15888888888', 'yqm@qq.com', '0', '0', 'admin', '2023-08-04 23:49:54', '', '2023-08-04 23:49:54', 1);
INSERT INTO `yqm_sys_dept` VALUES (200, 100, '0,100', '测试部门啊3', 1, '测试人员', '15871702127', 'test@qq.com', '0', '0', '1', '2023-08-09 22:44:30', '1', '2023-08-19 15:01:06', 1);
INSERT INTO `yqm_sys_dept` VALUES (201, 100, '0,100', '测试啊', 0, NULL, '1589999999', NULL, '0', '2', '1', '2023-08-16 22:43:33', '1', '2023-08-16 22:43:39', 1);

-- ----------------------------
-- Table structure for yqm_sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `yqm_sys_dict_data`;
CREATE TABLE `yqm_sys_dict_data`  (
  `dict_code` bigint NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yqm_sys_dict_data
-- ----------------------------
INSERT INTO `yqm_sys_dict_data` VALUES (1, 1, '男', '0', 'yqm_sys_user_sex', '', '', 'Y', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '性别男');
INSERT INTO `yqm_sys_dict_data` VALUES (2, 2, '女', '1', 'yqm_sys_user_sex', '', '', 'N', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '性别女');
INSERT INTO `yqm_sys_dict_data` VALUES (3, 3, '未知', '2', 'yqm_sys_user_sex', '', '', 'N', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '性别未知');
INSERT INTO `yqm_sys_dict_data` VALUES (4, 1, '显示', '0', 'yqm_sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '显示菜单');
INSERT INTO `yqm_sys_dict_data` VALUES (5, 2, '隐藏', '1', 'yqm_sys_show_hide', '', 'danger', 'N', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '隐藏菜单');
INSERT INTO `yqm_sys_dict_data` VALUES (6, 1, '正常', '0', 'yqm_sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '正常状态');
INSERT INTO `yqm_sys_dict_data` VALUES (7, 2, '停用', '1', 'yqm_sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '停用状态');
INSERT INTO `yqm_sys_dict_data` VALUES (8, 1, '正常', '0', 'yqm_sys_job_status', '', 'primary', 'Y', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '正常状态');
INSERT INTO `yqm_sys_dict_data` VALUES (9, 2, '暂停', '1', 'yqm_sys_job_status', '', 'danger', 'N', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '停用状态');
INSERT INTO `yqm_sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'yqm_sys_job_group', '', '', 'Y', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '默认分组');
INSERT INTO `yqm_sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'yqm_sys_job_group', '', '', 'N', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '系统分组');
INSERT INTO `yqm_sys_dict_data` VALUES (12, 1, '是', 'Y', 'yqm_sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '系统默认是');
INSERT INTO `yqm_sys_dict_data` VALUES (13, 2, '否', 'N', 'yqm_sys_yes_no', '', 'danger', 'N', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '系统默认否');
INSERT INTO `yqm_sys_dict_data` VALUES (14, 1, '通知', '1', 'yqm_sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '通知');
INSERT INTO `yqm_sys_dict_data` VALUES (15, 2, '公告', '2', 'yqm_sys_notice_type', '', 'success', 'N', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '公告');
INSERT INTO `yqm_sys_dict_data` VALUES (16, 1, '正常', '0', 'yqm_sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '正常状态');
INSERT INTO `yqm_sys_dict_data` VALUES (17, 2, '关闭', '1', 'yqm_sys_notice_status', '', 'danger', 'N', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '关闭状态');
INSERT INTO `yqm_sys_dict_data` VALUES (18, 99, '其他', '0', 'yqm_sys_oper_type', '', 'info', 'N', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '其他操作');
INSERT INTO `yqm_sys_dict_data` VALUES (19, 1, '新增', '1', 'yqm_sys_oper_type', '', 'info', 'N', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '新增操作');
INSERT INTO `yqm_sys_dict_data` VALUES (20, 2, '修改', '2', 'yqm_sys_oper_type', '', 'info', 'N', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '修改操作');
INSERT INTO `yqm_sys_dict_data` VALUES (21, 3, '删除', '3', 'yqm_sys_oper_type', '', 'danger', 'N', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '删除操作');
INSERT INTO `yqm_sys_dict_data` VALUES (22, 4, '授权', '4', 'yqm_sys_oper_type', '', 'primary', 'N', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '授权操作');
INSERT INTO `yqm_sys_dict_data` VALUES (23, 5, '导出', '5', 'yqm_sys_oper_type', '', 'warning', 'N', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '导出操作');
INSERT INTO `yqm_sys_dict_data` VALUES (24, 6, '导入', '6', 'yqm_sys_oper_type', '', 'warning', 'N', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '导入操作');
INSERT INTO `yqm_sys_dict_data` VALUES (25, 7, '强退', '7', 'yqm_sys_oper_type', '', 'danger', 'N', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '强退操作');
INSERT INTO `yqm_sys_dict_data` VALUES (26, 8, '生成代码', '8', 'yqm_sys_oper_type', '', 'warning', 'N', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '生成操作');
INSERT INTO `yqm_sys_dict_data` VALUES (27, 9, '清空数据', '9', 'yqm_sys_oper_type', '', 'danger', 'N', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '清空操作');
INSERT INTO `yqm_sys_dict_data` VALUES (28, 1, '成功', '0', 'yqm_sys_common_status', '', 'primary', 'N', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '正常状态');
INSERT INTO `yqm_sys_dict_data` VALUES (29, 2, '失败', '1', 'yqm_sys_common_status', '', 'danger', 'N', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '停用状态');

-- ----------------------------
-- Table structure for yqm_sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `yqm_sys_dict_type`;
CREATE TABLE `yqm_sys_dict_type`  (
  `dict_id` bigint NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yqm_sys_dict_type
-- ----------------------------
INSERT INTO `yqm_sys_dict_type` VALUES (1, '用户性别', 'yqm_sys_user_sex', '0', 'admin', '2023-08-04 23:49:54', '1', '2023-08-19 15:03:16', '用户性别列表');
INSERT INTO `yqm_sys_dict_type` VALUES (2, '菜单状态', 'yqm_sys_show_hide', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '菜单状态列表');
INSERT INTO `yqm_sys_dict_type` VALUES (3, '系统开关', 'yqm_sys_normal_disable', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '系统开关列表');
INSERT INTO `yqm_sys_dict_type` VALUES (4, '任务状态', 'yqm_sys_job_status', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '任务状态列表');
INSERT INTO `yqm_sys_dict_type` VALUES (5, '任务分组', 'yqm_sys_job_group', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '任务分组列表');
INSERT INTO `yqm_sys_dict_type` VALUES (6, '系统是否', 'yqm_sys_yes_no', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '系统是否列表');
INSERT INTO `yqm_sys_dict_type` VALUES (7, '通知类型', 'yqm_sys_notice_type', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '通知类型列表');
INSERT INTO `yqm_sys_dict_type` VALUES (8, '通知状态', 'yqm_sys_notice_status', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '通知状态列表');
INSERT INTO `yqm_sys_dict_type` VALUES (9, '操作类型', 'yqm_sys_oper_type', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '操作类型列表');
INSERT INTO `yqm_sys_dict_type` VALUES (10, '系统状态', 'yqm_sys_common_status', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '登录状态列表');

-- ----------------------------
-- Table structure for yqm_sys_job
-- ----------------------------
DROP TABLE IF EXISTS `yqm_sys_job`;
CREATE TABLE `yqm_sys_job`  (
  `job_id` bigint NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yqm_sys_job
-- ----------------------------
INSERT INTO `yqm_sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2023-08-04 23:49:54', '', NULL, '');
INSERT INTO `yqm_sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2023-08-04 23:49:54', '', NULL, '');
INSERT INTO `yqm_sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2023-08-04 23:49:54', '', NULL, '');

-- ----------------------------
-- Table structure for yqm_sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `yqm_sys_job_log`;
CREATE TABLE `yqm_sys_job_log`  (
  `job_log_id` bigint NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yqm_sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for yqm_sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `yqm_sys_logininfor`;
CREATE TABLE `yqm_sys_logininfor`  (
  `info_id` bigint NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE,
  INDEX `idx_yqm_sys_logininfor_s`(`status`) USING BTREE,
  INDEX `idx_yqm_sys_logininfor_lt`(`login_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yqm_sys_logininfor
-- ----------------------------

-- ----------------------------
-- Table structure for yqm_sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `yqm_sys_menu`;
CREATE TABLE `yqm_sys_menu`  (
  `menu_id` bigint NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路由参数',
  `is_frame` int NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  `path_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '路由名称',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2000 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yqm_sys_menu
-- ----------------------------
INSERT INTO `yqm_sys_menu` VALUES (1, '系统管理', 0, 1, 'system', NULL, '', 1, 0, 'M', '0', '0', '', 'system', 'admin', '2023-08-04 23:49:54', '', NULL, '系统管理目录', 'system');

-- ----------------------------
-- Table structure for yqm_sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `yqm_sys_notice`;
CREATE TABLE `yqm_sys_notice`  (
  `notice_id` int NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yqm_sys_notice
-- ----------------------------
INSERT INTO `yqm_sys_notice` VALUES (1, '温馨提醒：2018-07-01 亦秋美新版本发布啦', '2', 0xE696B0E78988E69CACE58685E5AEB9, '0', 'admin', '2023-08-04 23:49:54', '', NULL, '管理员');
INSERT INTO `yqm_sys_notice` VALUES (2, '维护通知：2018-07-01 亦秋美系统凌晨维护', '1', 0xE7BBB4E68AA4E58685E5AEB9, '0', 'admin', '2023-08-04 23:49:54', '', NULL, '管理员');

-- ----------------------------
-- Table structure for yqm_sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `yqm_sys_oper_log`;
CREATE TABLE `yqm_sys_oper_log`  (
  `oper_id` bigint NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime NULL DEFAULT NULL COMMENT '操作时间',
  `cost_time` bigint NULL DEFAULT 0 COMMENT '消耗时间',
  PRIMARY KEY (`oper_id`) USING BTREE,
  INDEX `idx_yqm_sys_oper_log_bt`(`business_type`) USING BTREE,
  INDEX `idx_yqm_sys_oper_log_s`(`status`) USING BTREE,
  INDEX `idx_yqm_sys_oper_log_ot`(`oper_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yqm_sys_oper_log
-- ----------------------------

-- ----------------------------
-- Table structure for yqm_sys_post
-- ----------------------------
DROP TABLE IF EXISTS `yqm_sys_post`;
CREATE TABLE `yqm_sys_post`  (
  `post_id` bigint NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yqm_sys_post
-- ----------------------------
INSERT INTO `yqm_sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2023-08-04 23:49:54', '', NULL, '', '0');
INSERT INTO `yqm_sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2023-08-04 23:49:54', '', NULL, '', '0');
INSERT INTO `yqm_sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2023-08-04 23:49:54', '', NULL, '', '0');
INSERT INTO `yqm_sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2023-08-04 23:49:54', '', NULL, '', '0');
INSERT INTO `yqm_sys_post` VALUES (6, 'test', '测试岗位', 1, '0', '1', '2023-08-17 20:17:23', '1', '2023-08-17 20:17:23', NULL, '0');
INSERT INTO `yqm_sys_post` VALUES (7, 'test01', '测试岗位01', 1, '0', '1', '2023-08-17 20:19:53', '1', '2023-08-17 20:19:53', NULL, '0');

-- ----------------------------
-- Table structure for yqm_sys_role
-- ----------------------------
DROP TABLE IF EXISTS `yqm_sys_role`;
CREATE TABLE `yqm_sys_role`  (
  `role_id` bigint NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `not_operation` int NULL DEFAULT 1 COMMENT '是否允许操作(0: 不允许 1: 允许 )',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 107 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yqm_sys_role
-- ----------------------------
INSERT INTO `yqm_sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '超级管理员', 0);
INSERT INTO `yqm_sys_role` VALUES (2, '普通角色', 'common', 2, '2', 1, 1, '0', '0', 'admin', '2023-08-04 23:49:54', '', NULL, '普通角色', 1);
INSERT INTO `yqm_sys_role` VALUES (105, '测试角色1', 'test', 1, '1', 1, 1, '1', '2', '1', '2023-08-07 22:52:53', '1', '2023-08-16 21:56:39', '测试用的', 1);

-- ----------------------------
-- Table structure for yqm_sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `yqm_sys_role_dept`;
CREATE TABLE `yqm_sys_role_dept`  (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `dept_id` bigint NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yqm_sys_role_dept
-- ----------------------------
INSERT INTO `yqm_sys_role_dept` VALUES (2, 100);
INSERT INTO `yqm_sys_role_dept` VALUES (2, 101);
INSERT INTO `yqm_sys_role_dept` VALUES (2, 105);

-- ----------------------------
-- Table structure for yqm_sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `yqm_sys_role_menu`;
CREATE TABLE `yqm_sys_role_menu`  (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `menu_id` bigint NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yqm_sys_role_menu
-- ----------------------------
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 2);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 3);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 4);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 100);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 101);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 102);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 103);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 104);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 105);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 106);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 107);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 108);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 109);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 110);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 111);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 112);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 113);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 114);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 115);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 116);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 117);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 500);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 501);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1000);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1001);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1002);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1003);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1004);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1005);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1006);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1007);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1008);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1009);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1010);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1011);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1012);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1013);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1014);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1015);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1016);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1017);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1018);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1019);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1020);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1021);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1022);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1023);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1024);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1025);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1026);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1027);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1028);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1029);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1030);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1031);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1032);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1033);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1034);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1035);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1036);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1037);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1038);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1039);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1040);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1041);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1042);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1043);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1044);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1045);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1046);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1047);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1048);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1049);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1050);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1051);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1052);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1053);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1054);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1055);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1056);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1057);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1058);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1059);
INSERT INTO `yqm_sys_role_menu` VALUES (2, 1060);

-- ----------------------------
-- Table structure for yqm_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `yqm_sys_user`;
CREATE TABLE `yqm_sys_user`  (
  `user_id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `not_operation` int NULL DEFAULT 1 COMMENT '是否允许操作(0: 不允许 1: 允许 )',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yqm_sys_user
-- ----------------------------
INSERT INTO `yqm_sys_user` VALUES (1, 103, 'admin', '管理员', '00', 'yqm@163.com', '15888888888', '1', 'http://localhost:8031/api/assets/1692532170345_032161.jpg', 'G841xo6Oc8vQUjwEjBpm+w==', '0', '0', '127.0.0.1', '2023-08-04 23:49:54', 'admin', '2023-08-04 23:49:54', '', '2023-08-04 23:49:54', '管理员', 0);
INSERT INTO `yqm_sys_user` VALUES (2, 105, 'yqm', '亦秋美', '00', 'yqm@qq.com', '15666666666', '1', 'http://localhost:8031/api/assets/1692532170345_032161.jpg', 'G841xo6Oc8vQUjwEjBpm+w==', '0', '0', '127.0.0.1', '2023-08-04 23:49:54', 'admin', '2023-08-04 23:49:54', '', '2023-08-04 23:49:54', '测试员', 0);
INSERT INTO `yqm_sys_user` VALUES (100, 200, 'test', '测试人员', '00', '', '', '0', 'http://localhost:8031/api/assets/1692532170345_032161.jpg', '', '0', '0', '', NULL, '', '2023-08-13 14:16:14', '1', '2023-08-20 20:35:26', '', 1);

-- ----------------------------
-- Table structure for yqm_sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `yqm_sys_user_post`;
CREATE TABLE `yqm_sys_user_post`  (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `post_id` bigint NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yqm_sys_user_post
-- ----------------------------
INSERT INTO `yqm_sys_user_post` VALUES (1, 1);
INSERT INTO `yqm_sys_user_post` VALUES (2, 2);
INSERT INTO `yqm_sys_user_post` VALUES (100, 1);
INSERT INTO `yqm_sys_user_post` VALUES (100, 2);
INSERT INTO `yqm_sys_user_post` VALUES (100, 6);
INSERT INTO `yqm_sys_user_post` VALUES (100, 7);

-- ----------------------------
-- Table structure for yqm_sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `yqm_sys_user_role`;
CREATE TABLE `yqm_sys_user_role`  (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yqm_sys_user_role
-- ----------------------------
INSERT INTO `yqm_sys_user_role` VALUES (1, 1);
INSERT INTO `yqm_sys_user_role` VALUES (2, 2);

SET FOREIGN_KEY_CHECKS = 1;
