package cn.yqm;

import cn.dev33.satoken.secure.SaSecureUtil;
import cn.yqm.common.YqmProperties;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/8/4
 */
@Slf4j
@SpringBootTest
public class YqmAdminApplicationTest {

    @Autowired
    private YqmProperties yqmProperties;

    @Test
    void genPassword() {
        // 定义秘钥和明文
        String key = yqmProperties.getCipherKey();
        String text = "123456";

        // 加密
        String ciphertext = SaSecureUtil.aesEncrypt(key, text);
        log.info("AES加密后：" + ciphertext);

        // 解密
        String text2 = SaSecureUtil.aesDecrypt(key, ciphertext);
        log.info("AES解密后：" + text2);
    }

}
