package cn.yqm.mapper;

import cn.yqm.entity.YqmSysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色信息表 Mapper 接口
 * </p>
 *
 * @author author
 * @since 2023-08-06
 */
public interface YqmSysRoleMapper extends BaseMapper<YqmSysRole> {

}
