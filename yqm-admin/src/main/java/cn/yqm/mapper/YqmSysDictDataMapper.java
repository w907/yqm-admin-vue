package cn.yqm.mapper;

import cn.yqm.entity.YqmSysDictData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 字典数据表 Mapper 接口
 * </p>
 *
 * @author weiximei
 * @since 2023-08-19
 */
public interface YqmSysDictDataMapper extends BaseMapper<YqmSysDictData> {

}
