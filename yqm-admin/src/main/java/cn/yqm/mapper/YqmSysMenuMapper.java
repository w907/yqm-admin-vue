package cn.yqm.mapper;

import cn.yqm.entity.YqmSysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 菜单权限表 Mapper 接口
 * </p>
 *
 * @author weiximei
 * @since 2023-08-21
 */
public interface YqmSysMenuMapper extends BaseMapper<YqmSysMenu> {

}
