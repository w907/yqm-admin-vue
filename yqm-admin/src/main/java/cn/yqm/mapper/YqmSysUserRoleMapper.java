package cn.yqm.mapper;

import cn.yqm.entity.YqmSysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户和角色关联表 Mapper 接口
 * </p>
 *
 * @author weiximei
 * @since 2023-08-19
 */
public interface YqmSysUserRoleMapper extends BaseMapper<YqmSysUserRole> {

}
