package cn.yqm.mapper;

import cn.yqm.entity.YqmSysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author author
 * @since 2023-08-04
 */
public interface YqmSysUserMapper extends BaseMapper<YqmSysUser> {

}
