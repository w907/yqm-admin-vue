package cn.yqm.mapper;

import cn.yqm.entity.YqmSysPost;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 岗位信息表 Mapper 接口
 * </p>
 *
 * @author weiximei
 * @since 2023-08-11
 */
public interface YqmSysPostMapper extends BaseMapper<YqmSysPost> {

}
