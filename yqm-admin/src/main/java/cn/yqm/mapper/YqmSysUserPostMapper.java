package cn.yqm.mapper;

import cn.yqm.entity.YqmSysUserPost;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户与岗位关联表 Mapper 接口
 * </p>
 *
 * @author weiximei
 * @since 2023-08-17
 */
public interface YqmSysUserPostMapper extends BaseMapper<YqmSysUserPost> {

}
