package cn.yqm.mapper;

import cn.yqm.entity.YqmSysDictType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 字典类型表 Mapper 接口
 * </p>
 *
 * @author weiximei
 * @since 2023-08-19
 */
public interface YqmSysDictTypeMapper extends BaseMapper<YqmSysDictType> {

}
