package cn.yqm.mapper;

import cn.yqm.entity.YqmSysRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色和菜单关联表 Mapper 接口
 * </p>
 *
 * @author weiximei
 * @since 2023-08-22
 */
public interface YqmSysRoleMenuMapper extends BaseMapper<YqmSysRoleMenu> {

}
