package cn.yqm.mapper;

import cn.yqm.entity.YqmSysDept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author author
 * @since 2023-08-08
 */
public interface YqmSysDeptMapper extends BaseMapper<YqmSysDept> {

}
