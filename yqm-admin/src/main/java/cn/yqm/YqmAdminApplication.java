package cn.yqm;

import cn.dev33.satoken.SaManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class YqmAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(YqmAdminApplication.class, args);
        log.info("启动成功，Sa-Token 配置如下：" + SaManager.getConfig());
    }

}
