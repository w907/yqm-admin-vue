package cn.yqm.req;

import lombok.Data;

/**
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/8/13
 */
@Data
public class YqmLoginReq {

    /**
     * 账号
     */
    private String account;

    /**
     * 密码
     */
    private String password;

    /**
     * 登录类型
     */
    private String loginType;

    /**
     * 是否记住密码
     */
    private Boolean remember;
}
