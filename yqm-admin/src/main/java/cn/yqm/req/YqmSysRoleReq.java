package cn.yqm.req;

import cn.yqm.common.req.BaseReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * <p>
 * 角色信息表
 * </p>
 *
 * @author author
 * @since 2023-08-06
 */
@Data
@Builder
@ApiModel(value = "YqmSysRole对象", description = "角色信息表")
public class YqmSysRoleReq extends BaseReq {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "角色ID")
    private Long roleId;

    @ApiModelProperty(value = "角色ID - 集合")
    private List<Long> roleIds;

    @ApiModelProperty(value = "角色名称")
    private String roleName;

    @ApiModelProperty(value = "角色名称-模糊")
    private String roleNameLike;

    @ApiModelProperty(value = "角色权限字符串")
    private String roleKey;

    @ApiModelProperty(value = "显示顺序")
    private Integer roleSort;

    @ApiModelProperty(value = "数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）")
    private String dataScope;

    @ApiModelProperty(value = "菜单树选择项是否关联显示")
    private Boolean menuCheckStrictly;

    @ApiModelProperty(value = "部门树选择项是否关联显示")
    private Boolean deptCheckStrictly;

    @ApiModelProperty(value = "角色状态（0正常 1停用）")
    private String status;

    @ApiModelProperty(value = "创建者")
    private String createBy;


    @ApiModelProperty(value = "更新者")
    private String updateBy;


    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "菜单id集合")
    private List<Long> menuIds;

}
