package cn.yqm.service;

import cn.yqm.entity.YqmSysUserRole;
import cn.yqm.req.YqmSysUserRoleReq;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户和角色关联表 服务类
 * </p>
 *
 * @author weiximei
 * @since 2023-08-19
 */
public interface IYqmSysUserRoleService extends IService<YqmSysUserRole> {

    /**
     * 查询条件
     *
     * @param req
     * @return
     */
    QueryWrapper<YqmSysUserRole> getQuery(YqmSysUserRoleReq req);

    /**
     * 根据用户id，查询出所有的角色id
     *
     * @param userId
     * @return
     */
    List<Long> getRoleIds(Long userId);

}
