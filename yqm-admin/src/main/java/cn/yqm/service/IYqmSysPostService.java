package cn.yqm.service;

import cn.yqm.entity.YqmSysPost;
import cn.yqm.req.YqmSysPostReq;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 岗位信息表 服务类
 * </p>
 *
 * @author weiximei
 * @since 2023-08-11
 */
public interface IYqmSysPostService extends IService<YqmSysPost> {

    /**
     * 查询条件
     *
     * @param req
     * @return
     */
    QueryWrapper<YqmSysPost> getQuery(YqmSysPostReq req);
}
