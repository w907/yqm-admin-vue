package cn.yqm.service;

import cn.yqm.entity.YqmSysDept;
import cn.yqm.req.YqmSysDeptReq;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 部门表 服务类
 * </p>
 *
 * @author author
 * @since 2023-08-08
 */
public interface IYqmSysDeptService extends IService<YqmSysDept> {


    /**
     * 查询条件
     *
     * @param req
     * @return
     */
    QueryWrapper<YqmSysDept> getQuery(YqmSysDeptReq req);

    /**
     * 查询部门子集（包含本部门）
     *
     * @param deptId
     * @return
     */
    List<YqmSysDept> getDeptChildren(Long deptId);

}
