package cn.yqm.service;

import cn.yqm.entity.YqmSysDictData;
import cn.yqm.req.YqmSysDictDataReq;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 字典数据表 服务类
 * </p>
 *
 * @author weiximei
 * @since 2023-08-19
 */
public interface IYqmSysDictDataService extends IService<YqmSysDictData> {


    /**
     * 查询条件
     *
     * @param req
     * @return
     */
    QueryWrapper<YqmSysDictData> getQuery(YqmSysDictDataReq req);

}
