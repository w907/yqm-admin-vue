package cn.yqm.service;

import cn.yqm.entity.YqmSysMenu;
import cn.yqm.req.YqmSysMenuReq;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 菜单权限表 服务类
 * </p>
 *
 * @author weiximei
 * @since 2023-08-21
 */
public interface IYqmSysMenuService extends IService<YqmSysMenu> {

    /**
     * 查询条件
     *
     * @param req
     * @return
     */
    QueryWrapper<YqmSysMenu> getQuery(YqmSysMenuReq req);

    /**
     * 查询菜单子集（包含本菜单）
     *
     * @param menuId
     * @return
     */
    List<YqmSysMenu> getMenuChildren(Long menuId);

    /**
     * 获取当前传递的所有菜单 id，包含 父级id 和 当前传递的id
     *
     * @param menuIds
     * @return
     */
    List<Long> getAllMenuId(List<Long> menuIds);

    /**
     * 根据菜单类型 查询
     *
     * @param menuType
     * @return
     */
    List<YqmSysMenu> getByMenuType(String menuType);

    /**
     * 根据菜单类型 查询
     *
     * @param menuType
     * @return
     */
    List<Long> getByMenuTypeAllMenuId(String menuType);

}
