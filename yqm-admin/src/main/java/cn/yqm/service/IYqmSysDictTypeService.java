package cn.yqm.service;

import cn.yqm.entity.YqmSysDictType;
import cn.yqm.req.YqmSysDictTypeReq;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 字典类型表 服务类
 * </p>
 *
 * @author weiximei
 * @since 2023-08-19
 */
public interface IYqmSysDictTypeService extends IService<YqmSysDictType> {

    /**
     * 查询条件
     *
     * @param req
     * @return
     */
    QueryWrapper<YqmSysDictType> getQuery(YqmSysDictTypeReq req);

}
