package cn.yqm.service;

import cn.yqm.entity.YqmSysRoleMenu;
import cn.yqm.req.YqmSysRoleMenuReq;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 角色和菜单关联表 服务类
 * </p>
 *
 * @author weiximei
 * @since 2023-08-22
 */
public interface IYqmSysRoleMenuService extends IService<YqmSysRoleMenu> {

    /**
     * 查询条件
     *
     * @param req
     * @return
     */
    QueryWrapper<YqmSysRoleMenu> getQuery(YqmSysRoleMenuReq req);

    /**
     * 根据 roleId 查询出所有的 menuId
     *
     * @param roleId
     * @return
     */
    List<Long> getMenuIds(Long roleId);


}
