package cn.yqm.service;

import cn.yqm.entity.YqmSysUserPost;
import cn.yqm.req.YqmSysUserPostReq;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户与岗位关联表 服务类
 * </p>
 *
 * @author weiximei
 * @since 2023-08-17
 */
public interface IYqmSysUserPostService extends IService<YqmSysUserPost> {

    /**
     * 查询条件
     *
     * @param req
     * @return
     */
    QueryWrapper<YqmSysUserPost> getQuery(YqmSysUserPostReq req);

    /**
     * 根据用户id，查询出所有的 岗位id
     *
     * @param userId
     * @return
     */
    List<Long> getPostIds(Long userId);

}
