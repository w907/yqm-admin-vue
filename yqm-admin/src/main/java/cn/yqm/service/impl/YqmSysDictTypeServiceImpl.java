package cn.yqm.service.impl;

import cn.yqm.entity.YqmSysDictType;
import cn.yqm.mapper.YqmSysDictTypeMapper;
import cn.yqm.req.YqmSysDictTypeReq;
import cn.yqm.service.IYqmSysDictTypeService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 字典类型表 服务实现类
 * </p>
 *
 * @author weiximei
 * @since 2023-08-19
 */
@Service
public class YqmSysDictTypeServiceImpl extends ServiceImpl<YqmSysDictTypeMapper, YqmSysDictType> implements IYqmSysDictTypeService {

    @Override
    public QueryWrapper<YqmSysDictType> getQuery(YqmSysDictTypeReq req) {
        QueryWrapper<YqmSysDictType> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(req.getDictName())) {
            queryWrapper.eq("dict_name", req.getDictName());
        }
        if (StringUtils.isNotBlank(req.getDictNameLike())) {
            queryWrapper.like("dict_name", req.getDictNameLike());
        }
        if (StringUtils.isNotBlank(req.getDictType())) {
            queryWrapper.eq("dict_type", req.getDictType());
        }
        queryWrapper.orderByDesc("update_time");
        return queryWrapper;
    }
}
