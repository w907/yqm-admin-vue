package cn.yqm.service.impl;

import cn.yqm.entity.YqmSysRole;
import cn.yqm.mapper.YqmSysRoleMapper;
import cn.yqm.req.YqmSysRoleReq;
import cn.yqm.service.IYqmSysRoleService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 角色信息表 服务实现类
 * </p>
 *
 * @author weiximei
 * @since 2023-08-06
 */
@Service
public class YqmSysRoleServiceImpl extends ServiceImpl<YqmSysRoleMapper, YqmSysRole> implements IYqmSysRoleService {

    @Override
    public QueryWrapper<YqmSysRole> getQuery(YqmSysRoleReq req) {
        QueryWrapper<YqmSysRole> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(req.getRoleName())) {
            queryWrapper.eq("role_name", req.getRoleName());
        }
        if (StringUtils.isNotBlank(req.getRoleKey())) {
            queryWrapper.eq("role_key", req.getRoleKey());
        }
        if (StringUtils.isNotBlank(req.getRoleNameLike())) {
            queryWrapper.like("role_name", req.getRoleNameLike());
        }
        queryWrapper.orderByAsc("role_sort");
        return queryWrapper;
    }

    @Override
    public List<String> getRoleKeys(List<Long> roleIds) {
        List<String> roleKeys = Lists.newArrayList();
        List<YqmSysRole> list = this.list(this.getQuery(YqmSysRoleReq.builder().roleIds(roleIds).build()));
        if (CollectionUtils.isNotEmpty(list)) {
            roleKeys = list.stream().map(YqmSysRole::getRoleKey).toList();
        }
        return roleKeys;
    }
}
