package cn.yqm.service.impl;

import cn.yqm.entity.YqmSysMenu;
import cn.yqm.mapper.YqmSysMenuMapper;
import cn.yqm.req.YqmSysMenuReq;
import cn.yqm.service.IYqmSysMenuService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * <p>
 * 菜单权限表 服务实现类
 * </p>
 *
 * @author weiximei
 * @since 2023-08-21
 */
@Service
public class YqmSysMenuServiceImpl extends ServiceImpl<YqmSysMenuMapper, YqmSysMenu> implements IYqmSysMenuService {


    @Override
    public QueryWrapper<YqmSysMenu> getQuery(YqmSysMenuReq req) {
        QueryWrapper<YqmSysMenu> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(req.getMenuName())) {
            queryWrapper.eq("menu_name", req.getMenuName());
        }
        if (Objects.nonNull(req.getParentId())) {
            queryWrapper.eq("parent_id", req.getParentId());
        }
        if (StringUtils.isNotBlank(req.getMenuNameLike())) {
            queryWrapper.like("menu_name", req.getMenuNameLike());
        }
        if (CollectionUtils.isNotEmpty(req.getMenuIds())) {
            queryWrapper.in("menu_id", req.getMenuIds());
        }
        if (Objects.nonNull(req.getMenuType())) {
            queryWrapper.eq("menu_type", req.getMenuType());
        }
        queryWrapper.orderByAsc("order_num");
        return queryWrapper;
    }

    @Override
    public List<YqmSysMenu> getMenuChildren(Long menuId) {
        YqmSysMenu yqmSysMenu = this.getById(menuId);
        List<YqmSysMenu> list = this.list(this.getQuery(YqmSysMenuReq.builder().parentId(menuId).build()));
        if (Objects.nonNull(list)) {
            list.add(yqmSysMenu);
        }
        return list;
    }

    @Override
    public List<Long> getAllMenuId(List<Long> menuIds) {
        List<YqmSysMenu> list = this.list(this.getQuery(YqmSysMenuReq.builder().menuIds(menuIds).build()));
        Set<Long> setHashSet = Sets.newHashSet();
        if (CollectionUtils.isNotEmpty(list)) {
            List<Long> parentIds = list.stream().map(YqmSysMenu::getParentId).toList();
            List<Long> ids = list.stream().map(YqmSysMenu::getMenuId).toList();

            setHashSet.addAll(parentIds);
            setHashSet.addAll(ids);
        }


        return Lists.newArrayList(setHashSet);
    }

    @Override
    public List<YqmSysMenu> getByMenuType(String menuType) {
        return this.list(this.getQuery(YqmSysMenuReq.builder().menuType(menuType).build()));
    }

    @Override
    public List<Long> getByMenuTypeAllMenuId(String menuType) {
        List<Long> menuIds = Lists.newArrayList();
        List<YqmSysMenu> list = this.getByMenuType(menuType);
        if (CollectionUtils.isNotEmpty(list)) {
            menuIds = list.stream().map(YqmSysMenu::getMenuId).toList();
        }
        return menuIds;
    }
}
