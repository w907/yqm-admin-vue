package cn.yqm.service.impl;

import cn.yqm.entity.YqmSysDept;
import cn.yqm.mapper.YqmSysDeptMapper;
import cn.yqm.req.YqmSysDeptReq;
import cn.yqm.service.IYqmSysDeptService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 部门表 服务实现类
 * </p>
 *
 * @author author
 * @since 2023-08-08
 */
@Service
public class YqmSysDeptServiceImpl extends ServiceImpl<YqmSysDeptMapper, YqmSysDept> implements IYqmSysDeptService {

    @Override
    public QueryWrapper<YqmSysDept> getQuery(YqmSysDeptReq req) {
        QueryWrapper<YqmSysDept> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(req.getDeptName())) {
            queryWrapper.eq("dept_name", req.getDeptName());
        }
        if (Objects.nonNull(req.getParentId())) {
            queryWrapper.eq("parent_id", req.getParentId());
        }
        if (StringUtils.isNotBlank(req.getDeptNameLike())) {
            queryWrapper.like("dept_name", req.getDeptNameLike());
        }
        if (StringUtils.isNotBlank(req.getAncestorLike())) {
            queryWrapper.like("ancestors", req.getAncestorLike());
        }
        queryWrapper.orderByDesc("update_time");
        return queryWrapper;
    }

    @Override
    public List<YqmSysDept> getDeptChildren(Long deptId) {
        YqmSysDept yqmSysDept = this.getById(deptId);
        List<YqmSysDept> like = this.list(this.getQuery(YqmSysDeptReq.builder().ancestorLike(String.valueOf(deptId)).build()));
        if (Objects.nonNull(yqmSysDept)) {
            like.add(yqmSysDept);
        }
        return like;
    }
}
