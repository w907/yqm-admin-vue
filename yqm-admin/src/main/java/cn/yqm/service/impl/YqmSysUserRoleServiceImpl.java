package cn.yqm.service.impl;

import cn.yqm.entity.YqmSysUserRole;
import cn.yqm.mapper.YqmSysUserRoleMapper;
import cn.yqm.req.YqmSysUserRoleReq;
import cn.yqm.service.IYqmSysUserRoleService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 用户和角色关联表 服务实现类
 * </p>
 *
 * @author weiximei
 * @since 2023-08-19
 */
@Service
public class YqmSysUserRoleServiceImpl extends ServiceImpl<YqmSysUserRoleMapper, YqmSysUserRole> implements IYqmSysUserRoleService {

    @Override
    public QueryWrapper<YqmSysUserRole> getQuery(YqmSysUserRoleReq req) {
        QueryWrapper<YqmSysUserRole> queryWrapper = new QueryWrapper<>();
        if (Objects.nonNull(req.getUserId())) {
            queryWrapper.eq("user_id", req.getUserId());
        }
        if (Objects.nonNull(req.getRoleId())) {
            queryWrapper.eq("role_id", req.getRoleId());
        }

        return queryWrapper;
    }

    @Override
    public List<Long> getRoleIds(Long userId) {
        List<Long> roleIds = Lists.newArrayList();
        List<YqmSysUserRole> yqmSysUserRoles = this.list(this.getQuery(YqmSysUserRoleReq.builder().userId(userId).build()));
        if (CollectionUtils.isNotEmpty(yqmSysUserRoles)) {
            roleIds = yqmSysUserRoles.stream().map(YqmSysUserRole::getRoleId).toList();
        }
        return roleIds;
    }
}
