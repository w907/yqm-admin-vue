package cn.yqm.service.impl;

import cn.yqm.entity.YqmSysUser;
import cn.yqm.mapper.YqmSysUserMapper;
import cn.yqm.req.YqmSysUserReq;
import cn.yqm.service.IYqmSysUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户信息表 服务实现类
 * </p>
 *
 * @author author
 * @since 2023-08-04
 */
@Service
public class YqmSysUserServiceImpl extends ServiceImpl<YqmSysUserMapper, YqmSysUser> implements IYqmSysUserService {

    @Override
    public QueryWrapper<YqmSysUser> getQuery(YqmSysUserReq req) {
        QueryWrapper<YqmSysUser> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(req.getUserName())) {
            queryWrapper.eq("user_name", req.getUserName());
        }
        if (StringUtils.isNotBlank(req.getNickNameLike())) {
            queryWrapper.like("nick_name", req.getNickNameLike());
        }
        if (StringUtils.isNotBlank(req.getNickName())) {
            queryWrapper.eq("nick_name", req.getNickName());
        }
        if (CollectionUtils.isNotEmpty(req.getDeptIdList())) {
            queryWrapper.in("dept_id", req.getDeptIdList());
        }

        queryWrapper.orderByDesc("update_time");
        return queryWrapper;
    }
}
