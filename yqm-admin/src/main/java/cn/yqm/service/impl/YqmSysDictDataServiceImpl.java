package cn.yqm.service.impl;

import cn.yqm.entity.YqmSysDictData;
import cn.yqm.mapper.YqmSysDictDataMapper;
import cn.yqm.req.YqmSysDictDataReq;
import cn.yqm.service.IYqmSysDictDataService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 字典数据表 服务实现类
 * </p>
 *
 * @author weiximei
 * @since 2023-08-19
 */
@Service
public class YqmSysDictDataServiceImpl extends ServiceImpl<YqmSysDictDataMapper, YqmSysDictData> implements IYqmSysDictDataService {

    @Override
    public QueryWrapper<YqmSysDictData> getQuery(YqmSysDictDataReq req) {
        QueryWrapper<YqmSysDictData> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(req.getDictType())) {
            queryWrapper.eq("dict_type", req.getDictType());
        }
        if (StringUtils.isNotBlank(req.getDictLabel())) {
            queryWrapper.like("dict_label", req.getDictLabel());
        }
        queryWrapper.orderByDesc("update_time");
        return queryWrapper;
    }
}
