package cn.yqm.service.impl;

import cn.yqm.entity.YqmSysUserPost;
import cn.yqm.mapper.YqmSysUserPostMapper;
import cn.yqm.req.YqmSysUserPostReq;
import cn.yqm.service.IYqmSysUserPostService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 用户与岗位关联表 服务实现类
 * </p>
 *
 * @author weiximei
 * @since 2023-08-17
 */
@Service
public class YqmSysUserPostServiceImpl extends ServiceImpl<YqmSysUserPostMapper, YqmSysUserPost> implements IYqmSysUserPostService {

    @Override
    public QueryWrapper<YqmSysUserPost> getQuery(YqmSysUserPostReq req) {
        QueryWrapper<YqmSysUserPost> queryWrapper = new QueryWrapper<>();
        if (Objects.nonNull(req.getUserId())) {
            queryWrapper.eq("user_id", req.getUserId());
        }
        if (Objects.nonNull(req.getPostId())) {
            queryWrapper.eq("post_id", req.getPostId());
        }

        return queryWrapper;
    }

    @Override
    public List<Long> getPostIds(Long userId) {
        List<Long> postIds = Lists.newArrayList();
        List<YqmSysUserPost> yqmSysUserPosts = this.list(this.getQuery(YqmSysUserPostReq.builder().userId(userId).build()));
        if (CollectionUtils.isNotEmpty(yqmSysUserPosts)) {
            postIds = yqmSysUserPosts.stream().map(YqmSysUserPost::getPostId).toList();
        }
        return postIds;
    }
}
