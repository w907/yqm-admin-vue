package cn.yqm.service.impl;

import cn.yqm.entity.YqmSysRoleMenu;
import cn.yqm.mapper.YqmSysRoleMenuMapper;
import cn.yqm.req.YqmSysRoleMenuReq;
import cn.yqm.service.IYqmSysRoleMenuService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 角色和菜单关联表 服务实现类
 * </p>
 *
 * @author weiximei
 * @since 2023-08-22
 */
@Service
public class YqmSysRoleMenuServiceImpl extends ServiceImpl<YqmSysRoleMenuMapper, YqmSysRoleMenu> implements IYqmSysRoleMenuService {

    @Override
    public QueryWrapper<YqmSysRoleMenu> getQuery(YqmSysRoleMenuReq req) {
        QueryWrapper<YqmSysRoleMenu> queryWrapper = new QueryWrapper<>();
        if (Objects.nonNull(req.getRoleId())) {
            queryWrapper.eq("role_id", req.getRoleId());
        }
        if (Objects.nonNull(req.getMenuId())) {
            queryWrapper.eq("menu_id", req.getMenuId());
        }
        return queryWrapper;
    }

    @Override
    public List<Long> getMenuIds(Long roleId) {
        List<Long> menuIds = Lists.newArrayList();
        List<YqmSysRoleMenu> list = this.list(this.getQuery(YqmSysRoleMenuReq.builder().roleId(roleId).build()));
        if (CollectionUtils.isNotEmpty(list)) {
            menuIds = list.stream().map(YqmSysRoleMenu::getMenuId).toList();
        }
        return menuIds;
    }
}
