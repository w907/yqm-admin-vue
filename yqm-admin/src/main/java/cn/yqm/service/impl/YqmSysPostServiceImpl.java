package cn.yqm.service.impl;

import cn.yqm.entity.YqmSysPost;
import cn.yqm.mapper.YqmSysPostMapper;
import cn.yqm.req.YqmSysPostReq;
import cn.yqm.service.IYqmSysPostService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 岗位信息表 服务实现类
 * </p>
 *
 * @author weiximei
 * @since 2023-08-11
 */
@Service
public class YqmSysPostServiceImpl extends ServiceImpl<YqmSysPostMapper, YqmSysPost> implements IYqmSysPostService {

    @Override
    public QueryWrapper<YqmSysPost> getQuery(YqmSysPostReq req) {
        QueryWrapper<YqmSysPost> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(req.getPostName())) {
            queryWrapper.eq("post_name", req.getPostName());
        }
        if (StringUtils.isNotBlank(req.getPostNameLike())) {
            queryWrapper.like("post_name", req.getPostNameLike());
        }
        queryWrapper.orderByDesc("update_time");
        return queryWrapper;
    }
}
