package cn.yqm.service;

import cn.yqm.entity.YqmSysRole;
import cn.yqm.req.YqmSysRoleReq;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 角色信息表 服务类
 * </p>
 *
 * @author weiximei
 * @since 2023-08-06
 */
public interface IYqmSysRoleService extends IService<YqmSysRole> {

    /**
     * 查询条件
     *
     * @param req
     * @return
     */
    QueryWrapper<YqmSysRole> getQuery(YqmSysRoleReq req);

    /**
     * 根据roleId集合，获取 角色权限字符串 集合
     *
     * @param roleIds
     * @return
     */
    List<String> getRoleKeys(List<Long> roleIds);

}
