package cn.yqm.service;

import cn.yqm.entity.YqmSysUser;
import cn.yqm.req.YqmSysUserReq;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author author
 * @since 2023-08-04
 */
public interface IYqmSysUserService extends IService<YqmSysUser> {

    /**
     * 查询条件
     *
     * @param req
     * @return
     */
    QueryWrapper<YqmSysUser> getQuery(YqmSysUserReq req);
}
