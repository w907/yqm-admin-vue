package cn.yqm.modules.admin.service;

import cn.hutool.core.bean.BeanUtil;
import cn.yqm.common.enums.YqmStatusEnum;
import cn.yqm.common.exception.YqmException;
import cn.yqm.dto.YqmSysDictDataDto;
import cn.yqm.entity.YqmSysDictData;
import cn.yqm.req.YqmSysDictDataReq;
import cn.yqm.service.IYqmSysDictDataService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * 字典 - 数据
 *
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/8/6
 */

@AllArgsConstructor
@Service
public class DictDataService {

    private final IYqmSysDictDataService iYqmSysDictDataService;

    /**
     * 分页查询
     *
     * @param req
     * @return
     */
    public IPage<YqmSysDictDataDto> page(YqmSysDictDataReq req) {
        IPage<YqmSysDictData> page = new Page<>(req.getPage(), req.getPageSize());
        IPage pageList = iYqmSysDictDataService.page(page, iYqmSysDictDataService.getQuery(req));
        pageList.setRecords(BeanUtil.copyToList(pageList.getRecords(), YqmSysDictDataDto.class));
        return pageList;
    }

    /**
     * 保存/或修改
     *
     * @param req
     * @return
     */
    public YqmSysDictDataDto save(YqmSysDictDataReq req) {
        YqmSysDictData yqmSysDictData = new YqmSysDictData();
        if (Objects.nonNull(req.getDictCode())) {
            yqmSysDictData = iYqmSysDictDataService.getById(req.getDictCode());
        }

        BeanUtil.copyProperties(req, yqmSysDictData);
        iYqmSysDictDataService.saveOrUpdate(yqmSysDictData);

        YqmSysDictDataDto sysDictDataDto = YqmSysDictDataDto.builder().build();
        BeanUtil.copyProperties(yqmSysDictData, sysDictDataDto);
        return sysDictDataDto;
    }

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    public YqmSysDictDataDto getById(String id) {
        YqmSysDictData yqmSysDictData = new YqmSysDictData();
        if (StringUtils.isNotBlank(id)) {
            yqmSysDictData = iYqmSysDictDataService.getById(id);
        }

        YqmSysDictDataDto sysDictDataDto = YqmSysDictDataDto.builder().build();
        BeanUtil.copyProperties(yqmSysDictData, sysDictDataDto);
        return sysDictDataDto;
    }

    /**
     * 删除
     *
     * @param ids
     * @return
     */
    public boolean delete(List<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            throw new YqmException("数据不存在！请检查");
        }
        return iYqmSysDictDataService.removeByIds(ids);
    }

    /**
     * 删除 - 根据 id
     *
     * @param id
     * @return
     */
    public boolean deleteById(String id) {
        if (StringUtils.isBlank(id)) {
            throw new YqmException("数据不存在！请检查");
        }
        return iYqmSysDictDataService.removeById(Long.valueOf(id));
    }

    /**
     * 启用/停用
     *
     * @param req
     * @return
     */
    public boolean enable(YqmSysDictDataReq req) {

        if (Objects.nonNull(YqmStatusEnum.getValue(req.getStatus()))) {
            YqmSysDictData yqmSysDictData = iYqmSysDictDataService.getById(req.getDictCode());
            yqmSysDictData.setStatus(req.getStatus());

            BeanUtil.copyProperties(yqmSysDictData, req);
            this.save(req);
        }

        return false;
    }


}
