package cn.yqm.modules.admin.controller;

import cn.yqm.common.ResultResponse;
import cn.yqm.modules.admin.service.DictTypeService;
import cn.yqm.req.YqmSysDictTypeReq;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 字典 - 类型
 *
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/8/6
 */

@AllArgsConstructor
@RestController
@RequestMapping("/dictType")
public class DictTypeController {

    private final DictTypeService dictService;

    /**
     * 分页查询
     *
     * @param req
     * @return
     */
    @GetMapping("/page")
    public ResultResponse page(YqmSysDictTypeReq req) {
        return ResultResponse.success(dictService.page(req));
    }

    /**
     * 保存/修改
     *
     * @param req
     * @return
     */
    @PostMapping("")
    public ResultResponse add(@RequestBody YqmSysDictTypeReq req) {
        return ResultResponse.success(dictService.save(req));
    }

    /**
     * 修改
     *
     * @param req
     * @return
     */
    @PutMapping("")
    public ResultResponse update(@RequestBody YqmSysDictTypeReq req) {
        return ResultResponse.success(dictService.save(req));
    }

    /**
     * 启用/停用
     *
     * @param req
     * @return
     */
    @PutMapping("/enable")
    public ResultResponse enable(@RequestBody YqmSysDictTypeReq req) {
        return ResultResponse.success(dictService.enable(req));
    }


    /**
     * 查询详情
     *
     * @param ids
     * @return
     */
    @DeleteMapping("/removeByIds")
    public ResultResponse delete(@RequestBody List<Long> ids) {
        return ResultResponse.success(dictService.delete(ids));
    }

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public ResultResponse getById(@PathVariable String id) {
        return ResultResponse.success(dictService.getById(id));
    }

    /**
     * 删除 - 根据id
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public ResultResponse delete(@PathVariable String id) {
        return ResultResponse.success(dictService.deleteById(id));
    }

}
