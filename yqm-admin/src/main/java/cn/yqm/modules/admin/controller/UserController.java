package cn.yqm.modules.admin.controller;

import cn.yqm.common.ResultResponse;
import cn.yqm.modules.admin.service.UserService;
import cn.yqm.req.YqmLoginReq;
import cn.yqm.req.YqmSysUserReq;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户
 *
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/8/4
 */

@AllArgsConstructor
@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    /**
     * 登录
     *
     * @return
     */
    @PostMapping("/login")
    public ResultResponse login(@RequestBody YqmLoginReq req) {
        return ResultResponse.success("登录成功", userService.login(req.getAccount(), req.getPassword(), req.getLoginType(), req.getRemember()));
    }

    /**
     * 获取用户信息
     *
     * @return
     */
    @GetMapping("/info")
    public ResultResponse info() {
        return ResultResponse.success(userService.getLoginInfo());
    }


    /**
     * 分页查询
     *
     * @param req
     * @return
     */
    @GetMapping("/page")
    public ResultResponse page(YqmSysUserReq req) {
        return ResultResponse.success(userService.page(req));
    }

    /**
     * 保存/修改
     *
     * @param req
     * @return
     */
    @PostMapping("")
    public ResultResponse add(@RequestBody YqmSysUserReq req) {
        return ResultResponse.success(userService.save(req));
    }

    /**
     * 修改
     *
     * @return
     */
    @PutMapping("")
    public ResultResponse update(@RequestBody YqmSysUserReq req) {
        return ResultResponse.success(userService.save(req));
    }

    /**
     * 启用/停用
     *
     * @param req
     * @return
     */
    @PutMapping("/enable")
    public ResultResponse enable(@RequestBody YqmSysUserReq req) {
        return ResultResponse.success(userService.enable(req));
    }

    /**
     * 删除
     *
     * @param ids
     * @return
     */
    @DeleteMapping("/removeByIds")
    public ResultResponse delete(@RequestBody List<Long> ids) {
        return ResultResponse.success(userService.delete(ids));
    }

    /**
     * 删除
     *
     * @return
     */
    @DeleteMapping("/{id}")
    public ResultResponse deleteById(@PathVariable String id) {
        return ResultResponse.success(userService.deleteById(id));
    }


    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public ResultResponse getById(@PathVariable String id) {
        return ResultResponse.success(userService.getById(id));
    }

    /**
     * 查询所有登录用户
     *
     * @return
     */
    @GetMapping("/getLoginUserAll")
    public ResultResponse getLoginUserAll() {
        userService.getLoginUserAll();
        return ResultResponse.success();
    }


}
