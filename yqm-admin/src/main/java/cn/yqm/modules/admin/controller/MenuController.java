package cn.yqm.modules.admin.controller;

import cn.yqm.common.ResultResponse;
import cn.yqm.modules.admin.service.MenuService;
import cn.yqm.req.YqmSysMenuReq;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 菜单
 *
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/8/6
 */

@AllArgsConstructor
@RestController
@RequestMapping("/menu")
public class MenuController {

    private final MenuService menuService;

    /**
     * 分页查询
     *
     * @param req
     * @return
     */
    @GetMapping("/page")
    public ResultResponse page(YqmSysMenuReq req) {
        return ResultResponse.success(menuService.page(req));
    }

    /**
     * 查询列表
     *
     * @param req
     * @return
     */
    @GetMapping("/list")
    public ResultResponse list(YqmSysMenuReq req) {
        return ResultResponse.success(menuService.list(req));
    }

    /**
     * 保存/修改
     *
     * @param req
     * @return
     */
    @PostMapping("")
    public ResultResponse add(@RequestBody YqmSysMenuReq req) {
        return ResultResponse.success(menuService.save(req));
    }

    /**
     * 修改
     *
     * @param req
     * @return
     */
    @PutMapping("")
    public ResultResponse update(@RequestBody YqmSysMenuReq req) {
        return ResultResponse.success(menuService.save(req));
    }

    /**
     * 启用/停用
     *
     * @param req
     * @return
     */
    @PutMapping("/enable")
    public ResultResponse enable(@RequestBody YqmSysMenuReq req) {
        return ResultResponse.success(menuService.enable(req));
    }


    /**
     * 查询详情
     *
     * @param ids
     * @return
     */
    @DeleteMapping("/removeByIds")
    public ResultResponse delete(@RequestBody List<Long> ids) {
        return ResultResponse.success(menuService.delete(ids));
    }

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public ResultResponse getById(@PathVariable String id) {
        return ResultResponse.success(menuService.getById(id));
    }

    /**
     * 删除 - 根据id
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public ResultResponse delete(@PathVariable String id) {
        return ResultResponse.success(menuService.deleteById(id));
    }

}
