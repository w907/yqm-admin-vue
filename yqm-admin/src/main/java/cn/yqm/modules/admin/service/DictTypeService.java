package cn.yqm.modules.admin.service;

import cn.hutool.core.bean.BeanUtil;
import cn.yqm.common.enums.YqmStatusEnum;
import cn.yqm.common.exception.YqmException;
import cn.yqm.dto.YqmSysDictTypeDto;
import cn.yqm.entity.YqmSysDictType;
import cn.yqm.req.YqmSysDictTypeReq;
import cn.yqm.service.IYqmSysDictTypeService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * 字典 - 类型
 *
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/8/6
 */

@AllArgsConstructor
@Service
public class DictTypeService {

    private final IYqmSysDictTypeService iYqmSysDictTypeService;

    /**
     * 分页查询
     *
     * @param req
     * @return
     */
    public IPage<YqmSysDictTypeDto> page(YqmSysDictTypeReq req) {
        IPage<YqmSysDictType> page = new Page<>(req.getPage(), req.getPageSize());
        IPage pageList = iYqmSysDictTypeService.page(page, iYqmSysDictTypeService.getQuery(req));
        pageList.setRecords(BeanUtil.copyToList(pageList.getRecords(), YqmSysDictTypeDto.class));
        return pageList;
    }

    /**
     * 保存/或修改
     *
     * @param req
     * @return
     */
    public YqmSysDictTypeDto save(YqmSysDictTypeReq req) {
        YqmSysDictType yqmSysDictType = new YqmSysDictType();
        if (Objects.nonNull(req.getDictId())) {
            yqmSysDictType = iYqmSysDictTypeService.getById(req.getDictId());
        }

        BeanUtil.copyProperties(req, yqmSysDictType);
        iYqmSysDictTypeService.saveOrUpdate(yqmSysDictType);

        YqmSysDictTypeDto sysDictTypeDto = YqmSysDictTypeDto.builder().build();
        BeanUtil.copyProperties(yqmSysDictType, sysDictTypeDto);
        return sysDictTypeDto;
    }

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    public YqmSysDictTypeDto getById(String id) {
        YqmSysDictType yqmSysDictType = new YqmSysDictType();
        if (StringUtils.isNotBlank(id)) {
            yqmSysDictType = iYqmSysDictTypeService.getById(id);
        }

        YqmSysDictTypeDto sysDictTypeDto = YqmSysDictTypeDto.builder().build();
        BeanUtil.copyProperties(yqmSysDictType, sysDictTypeDto);
        return sysDictTypeDto;
    }

    /**
     * 删除
     *
     * @param ids
     * @return
     */
    public boolean delete(List<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            throw new YqmException("数据不存在！请检查");
        }
        return iYqmSysDictTypeService.removeByIds(ids);
    }

    /**
     * 删除 - 根据 id
     *
     * @param id
     * @return
     */
    public boolean deleteById(String id) {
        if (StringUtils.isBlank(id)) {
            throw new YqmException("数据不存在！请检查");
        }
        return iYqmSysDictTypeService.removeById(Long.valueOf(id));
    }

    /**
     * 启用/停用
     *
     * @param req
     * @return
     */
    public boolean enable(YqmSysDictTypeReq req) {

        if (Objects.nonNull(YqmStatusEnum.getValue(req.getStatus()))) {
            YqmSysDictType yqmSysDictType = iYqmSysDictTypeService.getById(req.getDictId());
            yqmSysDictType.setStatus(req.getStatus());

            BeanUtil.copyProperties(yqmSysDictType, req);
            this.save(req);
        }

        return false;
    }


}
