package cn.yqm.modules.admin.controller;

import cn.yqm.common.ResultResponse;
import cn.yqm.modules.admin.service.DeptService;
import cn.yqm.req.YqmSysDeptReq;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 部门
 *
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/8/6
 */

@AllArgsConstructor
@RestController
@RequestMapping("/dept")
public class DeptController {

    private final DeptService deptService;

    /**
     * 分页查询
     *
     * @param req
     * @return
     */
    @GetMapping("/page")
    public ResultResponse page(YqmSysDeptReq req) {
        return ResultResponse.success(deptService.page(req));
    }

    /**
     * 查询列表
     *
     * @param req
     * @return
     */
    @GetMapping("/list")
    public ResultResponse list(YqmSysDeptReq req) {
        return ResultResponse.success(deptService.list(req));
    }

    /**
     * 保存/修改
     *
     * @param req
     * @return
     */
    @PostMapping("")
    public ResultResponse add(@RequestBody YqmSysDeptReq req) {
        return ResultResponse.success(deptService.save(req));
    }

    /**
     * 修改
     *
     * @param req
     * @return
     */
    @PutMapping("")
    public ResultResponse update(@RequestBody YqmSysDeptReq req) {
        return ResultResponse.success(deptService.save(req));
    }

    /**
     * 启用/停用
     *
     * @param req
     * @return
     */
    @PutMapping("/enable")
    public ResultResponse enable(@RequestBody YqmSysDeptReq req) {
        return ResultResponse.success(deptService.enable(req));
    }


    /**
     * 查询详情
     *
     * @param ids
     * @return
     */
    @DeleteMapping("/removeByIds")
    public ResultResponse delete(@RequestBody List<Long> ids) {
        return ResultResponse.success(deptService.delete(ids));
    }

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public ResultResponse getById(@PathVariable String id) {
        return ResultResponse.success(deptService.getById(id));
    }

    /**
     * 删除 - 根据id
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public ResultResponse delete(@PathVariable String id) {
        return ResultResponse.success(deptService.deleteById(id));
    }

}
