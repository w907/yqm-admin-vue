package cn.yqm.modules.admin.service;

import cn.hutool.core.bean.BeanUtil;
import cn.yqm.common.enums.YqmMenuEnum;
import cn.yqm.common.exception.YqmException;
import cn.yqm.dto.YqmSysRoleDto;
import cn.yqm.entity.YqmSysRole;
import cn.yqm.entity.YqmSysRoleMenu;
import cn.yqm.req.YqmSysRoleReq;
import cn.yqm.service.IYqmSysMenuService;
import cn.yqm.service.IYqmSysRoleMenuService;
import cn.yqm.service.IYqmSysRoleService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 角色
 *
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/8/6
 */

@AllArgsConstructor
@Service
public class RoleService {

    private final IYqmSysRoleService iYqmSysRoleService;
    private final IYqmSysRoleMenuService iYqmSysRoleMenuService;
    private final IYqmSysMenuService iYqmSysMenuService;

    /**
     * 分页查询
     *
     * @param req
     * @return
     */
    public IPage<YqmSysRoleDto> page(YqmSysRoleReq req) {
        IPage<YqmSysRole> page = new Page<>(req.getPage(), req.getPageSize());
        IPage pageList = iYqmSysRoleService.page(page, iYqmSysRoleService.getQuery(req));
        pageList.setRecords(BeanUtil.copyToList(pageList.getRecords(), YqmSysRoleDto.class));
        return pageList;
    }

    /**
     * 保存/或修改
     *
     * @param req
     * @return
     */
    public YqmSysRoleDto save(YqmSysRoleReq req) {
        YqmSysRole yqmSysRole = new YqmSysRole();
        if (Objects.nonNull(req.getRoleId())) {
            yqmSysRole = iYqmSysRoleService.getById(req.getRoleId());
        }

        BeanUtil.copyProperties(req, yqmSysRole);

        if (CollectionUtils.isNotEmpty(req.getMenuIds())) {

        }

        QueryWrapper<YqmSysRoleMenu> deleteRoleQuery = new QueryWrapper<>();
        deleteRoleQuery.eq("role_id", req.getRoleId());
        iYqmSysRoleMenuService.remove(deleteRoleQuery);

        if (CollectionUtils.isNotEmpty(req.getMenuIds())) {
            List<Long> menuIds = iYqmSysMenuService.getAllMenuId(req.getMenuIds());
            List<YqmSysRoleMenu> yqmSysUserPosts = menuIds.stream().map(e -> YqmSysRoleMenu.builder().menuId(e).roleId(req.getRoleId()).build()).collect(Collectors.toList());
            iYqmSysRoleMenuService.saveBatch(yqmSysUserPosts);
        }

        iYqmSysRoleService.saveOrUpdate(yqmSysRole);

        YqmSysRoleDto sysRoleDto = YqmSysRoleDto.builder().build();
        BeanUtil.copyProperties(yqmSysRole, sysRoleDto);
        return sysRoleDto;
    }

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    public YqmSysRoleDto getById(String id) {
        YqmSysRole yqmSysRole = new YqmSysRole();
        if (StringUtils.isNotBlank(id)) {
            yqmSysRole = iYqmSysRoleService.getById(Long.valueOf(id));
        }


        YqmSysRoleDto sysRoleDto = YqmSysRoleDto.builder().build();
        BeanUtil.copyProperties(yqmSysRole, sysRoleDto);

        // 排除 菜单类型为 目录级别的菜单
        List<Long> menuIds = iYqmSysRoleMenuService.getMenuIds(Long.valueOf(id));
        List<Long> mMenuIds = iYqmSysMenuService.getByMenuTypeAllMenuId(YqmMenuEnum.YqmMenuTypeEnum.M.getValue());
        sysRoleDto.setMenuIds(Lists.newArrayList(CollectionUtils.subtract(menuIds, mMenuIds)));
        return sysRoleDto;
    }

    /**
     * 删除
     *
     * @param ids
     * @return
     */
    public boolean delete(List<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            throw new YqmException("数据不存在！请检查");
        }
        return iYqmSysRoleService.removeByIds(ids);
    }

    /**
     * 删除 - 根据 id
     *
     * @param id
     * @return
     */
    public boolean deleteById(String id) {
        if (StringUtils.isBlank(id)) {
            throw new YqmException("数据不存在！请检查");
        }
        return iYqmSysRoleService.removeById(Long.valueOf(id));
    }


}
