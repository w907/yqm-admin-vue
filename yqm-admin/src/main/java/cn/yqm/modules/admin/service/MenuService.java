package cn.yqm.modules.admin.service;

import cn.hutool.core.bean.BeanUtil;
import cn.yqm.common.enums.YqmMenuEnum;
import cn.yqm.common.enums.YqmStatusEnum;
import cn.yqm.common.exception.YqmException;
import cn.yqm.dto.YqmSysMenuDto;
import cn.yqm.entity.YqmSysMenu;
import cn.yqm.req.YqmSysMenuReq;
import cn.yqm.service.IYqmSysMenuService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * 菜单
 *
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/8/6
 */

@AllArgsConstructor
@Service
public class MenuService {

    private final IYqmSysMenuService iYqmSysMenuService;

    /**
     * 分页查询
     *
     * @param req
     * @return
     */
    public IPage<YqmSysMenuDto> page(YqmSysMenuReq req) {
        IPage<YqmSysMenu> page = new Page<>(req.getPage(), req.getPageSize());
        IPage pageList = iYqmSysMenuService.page(page, iYqmSysMenuService.getQuery(req));
        pageList.setRecords(BeanUtil.copyToList(pageList.getRecords(), YqmSysMenuDto.class));
        return pageList;
    }

    /**
     * 查询列表
     *
     * @param req
     * @return
     */
    public List<YqmSysMenuDto> list(YqmSysMenuReq req) {
        if (Objects.isNull(req.getParentId()) && StringUtils.isEmpty(req.getMenuNameLike())) {
            req.setParentId(0L);
        }
        List<YqmSysMenu> list = iYqmSysMenuService.list(iYqmSysMenuService.getQuery(req));
        if (CollectionUtils.isEmpty(list)) {
            return null; // Lists.newArrayList();
        }
        List<YqmSysMenuDto> dtoList = BeanUtil.copyToList(list, YqmSysMenuDto.class);
        dtoList.forEach(e -> {
            e.setChildren(this.list(YqmSysMenuReq.builder().parentId(e.getMenuId()).build()));
        });
        return dtoList;
    }

    /**
     * 保存/或修改
     *
     * @param req
     * @return
     */
    public YqmSysMenuDto save(YqmSysMenuReq req) {
        YqmSysMenu yqmSysMenu = new YqmSysMenu();
        if (Objects.nonNull(req.getMenuId())) {
            yqmSysMenu = iYqmSysMenuService.getById(req.getMenuId());
        }

        BeanUtil.copyProperties(req, yqmSysMenu);

        Long parentId = yqmSysMenu.getParentId();
        YqmSysMenu parent = iYqmSysMenuService.getById(parentId);
        if (Objects.isNull(parent) && 0 != parentId) {
            throw new YqmException("父级菜单不存在，请检查");
        }

        // 如果是 目录级别
        if (YqmMenuEnum.YqmMenuTypeEnum.M.getValue().equals(req.getMenuType())) {
//            String menuPath = req.getPath();
//            if (!menuPath.startsWith("/")) {
//                yqmSysMenu.setPath("/" + menuPath);
//            }
        }

        iYqmSysMenuService.saveOrUpdate(yqmSysMenu);

        YqmSysMenuDto sysRoleDto = YqmSysMenuDto.builder().build();
        BeanUtil.copyProperties(yqmSysMenu, sysRoleDto);
        return sysRoleDto;
    }

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    public YqmSysMenuDto getById(String id) {
        YqmSysMenu yqmSysMenu = new YqmSysMenu();
        if (StringUtils.isNotBlank(id)) {
            yqmSysMenu = iYqmSysMenuService.getById(id);
        }

        YqmSysMenuDto sysRoleDto = YqmSysMenuDto.builder().build();
        BeanUtil.copyProperties(yqmSysMenu, sysRoleDto);
        return sysRoleDto;
    }

    /**
     * 删除
     *
     * @param ids
     * @return
     */
    public boolean delete(List<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            throw new YqmException("数据不存在！请检查");
        }
        return iYqmSysMenuService.removeByIds(ids);
    }

    /**
     * 启用/停用
     *
     * @param req
     * @return
     */
    public boolean enable(YqmSysMenuReq req) {

        if (Objects.nonNull(YqmStatusEnum.getValue(req.getStatus()))) {
            YqmSysMenu yqmSysMenu = iYqmSysMenuService.getById(req.getMenuId());
            yqmSysMenu.setStatus(req.getStatus());

            BeanUtil.copyProperties(yqmSysMenu, req);
            this.save(req);
        }

        return false;
    }

    /**
     * 根据id - 删除
     *
     * @param id
     * @return
     */
    public boolean deleteById(String id) {
        return this.delete(Lists.newArrayList(Long.valueOf(id)));
    }
}
