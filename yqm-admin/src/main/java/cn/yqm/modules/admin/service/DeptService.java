package cn.yqm.modules.admin.service;

import cn.hutool.core.bean.BeanUtil;
import cn.yqm.common.enums.YqmStatusEnum;
import cn.yqm.common.exception.YqmException;
import cn.yqm.dto.YqmSysDeptDto;
import cn.yqm.entity.YqmSysDept;
import cn.yqm.req.YqmSysDeptReq;
import cn.yqm.service.IYqmSysDeptService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * 角色
 *
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/8/6
 */

@AllArgsConstructor
@Service
public class DeptService {

    private final IYqmSysDeptService iYqmSysDeptService;

    /**
     * 分页查询
     *
     * @param req
     * @return
     */
    public IPage<YqmSysDeptDto> page(YqmSysDeptReq req) {
        IPage<YqmSysDept> page = new Page<>(req.getPage(), req.getPageSize());
        IPage pageList = iYqmSysDeptService.page(page, iYqmSysDeptService.getQuery(req));
        pageList.setRecords(BeanUtil.copyToList(pageList.getRecords(), YqmSysDeptDto.class));
        return pageList;
    }

    /**
     * 查询列表
     *
     * @param req
     * @return
     */
    public List<YqmSysDeptDto> list(YqmSysDeptReq req) {
        if (Objects.isNull(req.getParentId()) && StringUtils.isEmpty(req.getDeptNameLike())) {
            req.setParentId(0L);
        }
        List<YqmSysDept> list = iYqmSysDeptService.list(iYqmSysDeptService.getQuery(req));
        if (CollectionUtils.isEmpty(list)) {
            return null; // Lists.newArrayList();
        }
        List<YqmSysDeptDto> dtoList = BeanUtil.copyToList(list, YqmSysDeptDto.class);
        dtoList.forEach(e -> {
            e.setChildren(this.list(YqmSysDeptReq.builder().parentId(e.getDeptId()).build()));
        });
        return dtoList;
    }

    /**
     * 保存/或修改
     *
     * @param req
     * @return
     */
    public YqmSysDeptDto save(YqmSysDeptReq req) {
        YqmSysDept yqmSysDept = new YqmSysDept();
        if (Objects.nonNull(req.getDeptId())) {
            yqmSysDept = iYqmSysDeptService.getById(req.getDeptId());
        }

        BeanUtil.copyProperties(req, yqmSysDept);

        Long parentId = yqmSysDept.getParentId();
        YqmSysDept parent = iYqmSysDeptService.getById(parentId);
        if (Objects.nonNull(parent)) {
            List<String> ancestorList = Lists.newArrayList(StringUtils.split(parent.getAncestors(), ","));
            ancestorList.add(String.valueOf(parentId));
            yqmSysDept.setAncestors(StringUtils.join(Sets.newHashSet(ancestorList), ","));
        }

        iYqmSysDeptService.saveOrUpdate(yqmSysDept);

        YqmSysDeptDto sysRoleDto = YqmSysDeptDto.builder().build();
        BeanUtil.copyProperties(yqmSysDept, sysRoleDto);
        return sysRoleDto;
    }

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    public YqmSysDeptDto getById(String id) {
        YqmSysDept yqmSysDept = new YqmSysDept();
        if (StringUtils.isNotBlank(id)) {
            yqmSysDept = iYqmSysDeptService.getById(id);
        }

        YqmSysDeptDto sysRoleDto = YqmSysDeptDto.builder().build();
        BeanUtil.copyProperties(yqmSysDept, sysRoleDto);
        return sysRoleDto;
    }

    /**
     * 删除
     *
     * @param ids
     * @return
     */
    public boolean delete(List<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            throw new YqmException("数据不存在！请检查");
        }
        return iYqmSysDeptService.removeByIds(ids);
    }

    /**
     * 启用/停用
     *
     * @param req
     * @return
     */
    public boolean enable(YqmSysDeptReq req) {

        if (Objects.nonNull(YqmStatusEnum.getValue(req.getStatus()))) {
            YqmSysDept yqmSysDept = iYqmSysDeptService.getById(req.getDeptId());
            yqmSysDept.setStatus(req.getStatus());

            BeanUtil.copyProperties(yqmSysDept, req);
            this.save(req);
        }

        return false;
    }

    /**
     * 根据id - 删除
     *
     * @param id
     * @return
     */
    public boolean deleteById(String id) {
        return this.delete(Lists.newArrayList(Long.valueOf(id)));
    }
}
