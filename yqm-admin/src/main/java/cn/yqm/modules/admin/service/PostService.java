package cn.yqm.modules.admin.service;

import cn.hutool.core.bean.BeanUtil;
import cn.yqm.common.enums.YqmStatusEnum;
import cn.yqm.common.exception.YqmException;
import cn.yqm.dto.YqmSysPostDto;
import cn.yqm.entity.YqmSysPost;
import cn.yqm.req.YqmSysPostReq;
import cn.yqm.service.IYqmSysPostService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * 岗位
 *
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/8/6
 */

@AllArgsConstructor
@Service
public class PostService {

    private final IYqmSysPostService iYqmSysPostService;

    /**
     * 分页查询
     *
     * @param req
     * @return
     */
    public IPage<YqmSysPostDto> page(YqmSysPostReq req) {
        IPage<YqmSysPost> page = new Page<>(req.getPage(), req.getPageSize());
        IPage pageList = iYqmSysPostService.page(page, iYqmSysPostService.getQuery(req));
        pageList.setRecords(BeanUtil.copyToList(pageList.getRecords(), YqmSysPostDto.class));
        return pageList;
    }

    /**
     * 保存/或修改
     *
     * @param req
     * @return
     */
    public YqmSysPostDto save(YqmSysPostReq req) {
        YqmSysPost yqmSysPost = new YqmSysPost();
        if (Objects.nonNull(req.getPostId())) {
            yqmSysPost = iYqmSysPostService.getById(req.getPostId());
        }

        BeanUtil.copyProperties(req, yqmSysPost);
        iYqmSysPostService.saveOrUpdate(yqmSysPost);

        YqmSysPostDto sysPostDto = YqmSysPostDto.builder().build();
        BeanUtil.copyProperties(yqmSysPost, sysPostDto);
        return sysPostDto;
    }

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    public YqmSysPostDto getById(String id) {
        YqmSysPost yqmSysPost = new YqmSysPost();
        if (StringUtils.isNotBlank(id)) {
            yqmSysPost = iYqmSysPostService.getById(id);
        }

        YqmSysPostDto sysPostDto = YqmSysPostDto.builder().build();
        BeanUtil.copyProperties(yqmSysPost, sysPostDto);
        return sysPostDto;
    }

    /**
     * 删除
     *
     * @param ids
     * @return
     */
    public boolean removeByIds(List<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            throw new YqmException("数据不存在！请检查");
        }
        return iYqmSysPostService.removeByIds(ids);
    }

    /**
     * 启用/停用
     *
     * @param req
     * @return
     */
    public boolean enable(YqmSysPostReq req) {

        if (Objects.nonNull(YqmStatusEnum.getValue(req.getStatus()))) {
            YqmSysPost yqmSysPost = iYqmSysPostService.getById(req.getPostId());
            yqmSysPost.setStatus(req.getStatus());

            BeanUtil.copyProperties(yqmSysPost, req);
            this.save(req);
        }

        return false;
    }


    /**
     * 删除
     *
     * @return
     */
    public boolean removeById(String id) {
        if (StringUtils.isBlank(id)) {
            throw new YqmException("数据不存在！请检查");
        }
        return iYqmSysPostService.removeById(Long.valueOf(id));
    }
}
