package cn.yqm.modules.admin.controller;

import cn.yqm.common.ResultResponse;
import cn.yqm.modules.admin.service.PostService;
import cn.yqm.req.YqmSysPostReq;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 岗位
 *
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/8/6
 */

@AllArgsConstructor
@RestController
@RequestMapping("/post")
public class PostController {

    private final PostService postService;

    /**
     * 分页查询
     *
     * @param req
     * @return
     */
    @GetMapping("/page")
    public ResultResponse page(YqmSysPostReq req) {
        return ResultResponse.success(postService.page(req));
    }

    /**
     * 保存/修改
     *
     * @param req
     * @return
     */
    @PostMapping("")
    public ResultResponse add(@RequestBody YqmSysPostReq req) {
        return ResultResponse.success(postService.save(req));
    }

    /**
     * 修改
     *
     * @param req
     * @return
     */
    @PutMapping("")
    public ResultResponse update(@RequestBody YqmSysPostReq req) {
        return ResultResponse.success(postService.save(req));
    }

    /**
     * 启用/停用
     *
     * @param req
     * @return
     */
    @PutMapping("/enable")
    public ResultResponse enable(@RequestBody YqmSysPostReq req) {
        return ResultResponse.success(postService.enable(req));
    }


    /**
     * 删除
     *
     * @param ids
     * @return
     */
    @DeleteMapping("/removeByIds")
    public ResultResponse removeByIds(@RequestBody List<Long> ids) {
        return ResultResponse.success(postService.removeByIds(ids));
    }

    /**
     * 删除
     *
     * @return
     */
    @DeleteMapping("/{id}")
    public ResultResponse removeById(@PathVariable String id) {
        return ResultResponse.success(postService.removeById(id));
    }

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public ResultResponse getById(@PathVariable String id) {
        return ResultResponse.success(postService.getById(id));
    }


}
