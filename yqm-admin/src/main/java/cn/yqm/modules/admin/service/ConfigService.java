package cn.yqm.modules.admin.service;

import cn.hutool.core.bean.BeanUtil;
import cn.yqm.common.entity.YqmSysConfig;
import cn.yqm.common.enums.YqmStatusEnum;
import cn.yqm.common.exception.YqmException;
import cn.yqm.common.req.YqmSysConfigReq;
import cn.yqm.common.service.IYqmSysConfigService;
import cn.yqm.dto.YqmSysConfigDto;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * 参数配置
 *
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/8/6
 */

@AllArgsConstructor
@Service
public class ConfigService {

    private final IYqmSysConfigService iYqmSysConfigService;

    /**
     * 分页查询
     *
     * @param req
     * @return
     */
    public IPage<YqmSysConfigDto> page(YqmSysConfigReq req) {
        IPage<YqmSysConfig> page = new Page<>(req.getPage(), req.getPageSize());
        IPage pageList = iYqmSysConfigService.page(page, iYqmSysConfigService.getQuery(req));
        pageList.setRecords(BeanUtil.copyToList(pageList.getRecords(), YqmSysConfigDto.class));
        return pageList;
    }

    /**
     * 保存/或修改
     *
     * @param req
     * @return
     */
    public YqmSysConfigDto save(YqmSysConfigReq req) {
        YqmSysConfig yqmSysConfig = new YqmSysConfig();
        if (Objects.nonNull(req.getConfigId())) {
            yqmSysConfig = iYqmSysConfigService.getById(req.getConfigId());
        }

        BeanUtil.copyProperties(req, yqmSysConfig);
        iYqmSysConfigService.saveOrUpdate(yqmSysConfig);

        YqmSysConfigDto sysConfigDto = YqmSysConfigDto.builder().build();
        BeanUtil.copyProperties(yqmSysConfig, sysConfigDto);
        return sysConfigDto;
    }

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    public YqmSysConfigDto getById(String id) {
        YqmSysConfig yqmSysConfig = new YqmSysConfig();
        if (StringUtils.isNotBlank(id)) {
            yqmSysConfig = iYqmSysConfigService.getById(id);
        }

        YqmSysConfigDto sysConfigDto = YqmSysConfigDto.builder().build();
        BeanUtil.copyProperties(yqmSysConfig, sysConfigDto);
        return sysConfigDto;
    }

    /**
     * 删除
     *
     * @param ids
     * @return
     */
    public boolean delete(List<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            throw new YqmException("数据不存在！请检查");
        }
        return iYqmSysConfigService.removeByIds(ids);
    }

    /**
     * 删除 - 根据 id
     *
     * @param id
     * @return
     */
    public boolean deleteById(String id) {
        if (StringUtils.isBlank(id)) {
            throw new YqmException("数据不存在！请检查");
        }
        return iYqmSysConfigService.removeById(Long.valueOf(id));
    }

    /**
     * 启用/停用
     *
     * @param req
     * @return
     */
    public boolean enable(YqmSysConfigReq req) {

        if (Objects.nonNull(YqmStatusEnum.getValue(req.getStatus()))) {
            YqmSysConfig yqmSysConfig = iYqmSysConfigService.getById(req.getConfigId());
            yqmSysConfig.setStatus(req.getStatus());

            BeanUtil.copyProperties(yqmSysConfig, req);
            this.save(req);
        }

        return false;
    }


}
