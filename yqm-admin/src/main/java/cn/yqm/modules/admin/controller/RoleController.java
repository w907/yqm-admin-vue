package cn.yqm.modules.admin.controller;

import cn.yqm.common.ResultResponse;
import cn.yqm.modules.admin.service.RoleService;
import cn.yqm.req.YqmSysRoleReq;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 角色
 *
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/8/6
 */

@AllArgsConstructor
@RestController
@RequestMapping("/role")
public class RoleController {

    private final RoleService roleService;

    /**
     * 分页查询
     *
     * @param req
     * @return
     */
    @GetMapping("/page")
    public ResultResponse page(YqmSysRoleReq req) {
        return ResultResponse.success(roleService.page(req));
    }

    /**
     * 保存/修改
     *
     * @param req
     * @return
     */
    @PostMapping("")
    public ResultResponse add(@RequestBody YqmSysRoleReq req) {
        return ResultResponse.success(roleService.save(req));
    }

    /**
     * 修改
     *
     * @param req
     * @return
     */
    @PutMapping("")
    public ResultResponse update(@RequestBody YqmSysRoleReq req) {
        return ResultResponse.success(roleService.save(req));
    }

    /**
     * 删除 - 根据id
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public ResultResponse delete(@PathVariable String id) {
        return ResultResponse.success(roleService.deleteById(id));
    }

    /**
     * 删除
     *
     * @param ids
     * @return
     */
    @DeleteMapping("/removeByIds")
    public ResultResponse removeByIds(@RequestBody List<Long> ids) {
        return ResultResponse.success(roleService.delete(ids));
    }

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public ResultResponse getById(@PathVariable String id) {
        return ResultResponse.success(roleService.getById(id));
    }


}
