package cn.yqm.common;

/**
 * 登录接口
 *
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/7/23
 */
public interface LoginInterface {

    /**
     * 登录类型
     *
     * @return
     */
    String loginType();

    /**
     * 账号密码
     *
     * @param account
     * @param password
     * @param remember
     * @return
     */
    Object login(String account, String password, Boolean remember);

}
