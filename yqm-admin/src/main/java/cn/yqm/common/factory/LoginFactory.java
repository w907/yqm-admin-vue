package cn.yqm.common.factory;

import cn.yqm.common.LoginInterface;
import cn.yqm.common.YqmSpringUtils;
import cn.yqm.common.exception.YqmException;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 登录工厂
 *
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/7/23
 */
public class LoginFactory {

    private static final Map<String, LoginInterface> _map = Maps.newConcurrentMap();

    static {
        List<LoginInterface> loader = YqmSpringUtils.getBeanList(LoginInterface.class);
        loader.forEach(LoginFactory::setMap);
    }

    private LoginFactory() {
    }

    public static LoginInterface getSingle(String loginType) {
        if (StringUtils.isEmpty(loginType)) {
            throw new YqmException("未找到登录类型！请检查");
        }
        LoginInterface login = _map.get(loginType);
        if (Objects.isNull(login)) {
            throw new YqmException("未找到登录类型！请检查");
        }
        return login;
    }

    static void setMap(LoginInterface obj) {
        _map.put(obj.loginType(), obj);
    }

}
