package cn.yqm.common.factory;

import cn.yqm.common.UploadInterface;
import cn.yqm.common.YqmSpringUtils;
import cn.yqm.common.exception.YqmException;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 上传工厂
 *
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/7/23
 */
public class UploadFactory {

    private static final Map<String, UploadInterface> _map = Maps.newConcurrentMap();

    static {
        List<UploadInterface> loader = YqmSpringUtils.getBeanList(UploadInterface.class);
        loader.forEach(UploadFactory::setMap);
    }

    private UploadFactory() {
    }

    public static UploadInterface getSingle(String loginType) {
        if (StringUtils.isEmpty(loginType)) {
            throw new YqmException("未找到上传类型！请检查");
        }
        UploadInterface upload = _map.get(loginType);
        if (Objects.isNull(upload)) {
            throw new YqmException("未找到上传类型！请检查");
        }
        return upload;
    }

    static void setMap(UploadInterface obj) {
        _map.put(obj.uploadType(), obj);
    }

}
