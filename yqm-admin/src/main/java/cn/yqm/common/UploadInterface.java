package cn.yqm.common;

/**
 * 上传接口
 *
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/8/20
 */
public interface UploadInterface {

    /**
     * 上传类型
     *
     * @return
     */
    String uploadType();

    /**
     * 上传
     *
     * @param bytes          文件流
     * @param originFileName 文件名
     * @return
     */
    String upload(byte[] bytes, String originFileName);

}
