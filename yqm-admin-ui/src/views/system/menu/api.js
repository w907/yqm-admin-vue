import {request} from '@/utils'

export default {
    menuPage: (params = {}) => request.get('/menu/page', {params}),
    menuList: (params = {}) => request.get('/menu/list', {params}),
    menuGetById: (id) => request.get(`/menu/${id}`),
    addMenu: (data) => request.post(`/menu`, data),
    updateMenu: (data) => request.put(`/menu`, data),
    deleteMenu: (id) => request.delete(`/menu/${id}`),
}
