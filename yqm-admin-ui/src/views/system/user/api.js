import {request} from '@/utils'

export default {
    userPage: (params = {}) => request.get('/user/page', {params}),
    userGetById: (id) => request.get(`/user/${id}`),
    addUser: (data) => request.post(`/user`, data),
    updateUser: (data) => request.put(`/user`, data),
    deleteUser: (id) => request.delete(`/user/${id}`),
    deptList: (params = {}) => request.get('/dept/list', {params}),
    postPage: (params = {}) => request.get('/post/page', {params}),
    rolePage: (params = {}) => request.get('/role/page', {params}),
}
