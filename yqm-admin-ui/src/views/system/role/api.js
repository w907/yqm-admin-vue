import {request} from '@/utils'

export default {
    rolePage: (params = {}) => request.get('/role/page', {params}),
    roleGetById: (id) => request.get(`/role/${id}`),
    addRole: (data) => request.post(`/role`, data),
    updateRole: (data) => request.put(`/role`, data),
    deleteRole: (id) => request.delete(`/role/${id}`),
    menuList: (params = {}) => request.get('/menu/list', {params}),
}
