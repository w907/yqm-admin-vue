import {request} from '@/utils'

export default {
    dictTypePage: (params = {}) => request.get('/dictType/page', {params}),
    dictTypeGetById: (id) => request.get(`/dictType/${id}`),
    addDictType: (data) => request.post(`/dictType`, data),
    updateDictType: (data) => request.put(`/dictType`, data),
    deleteDictType: (id) => request.delete(`/dictType/${id}`),
}
