import {request} from '@/utils'

export default {
    postPage: (params = {}) => request.get('/post/page', {params}),
    postGetById: (id) => request.get(`/post/${id}`),
    addPost: (data) => request.post(`/post`, data),
    updatePost: (data) => request.put(`/post`, data),
    deletePost: (id) => request.delete(`/post/${id}`),
}
