import {request} from '@/utils'

export default {
    dictDataPage: (params = {}) => request.get('/dictData/page', {params}),
    dictDataGetById: (id) => request.get(`/dictData/${id}`),
    addDictType: (data) => request.post(`/dictData`, data),
    updateDictType: (data) => request.put(`/dictData`, data),
    deleteDictType: (id) => request.delete(`/dictData/${id}`),
    dictTypePage: (params = {}) => request.get('/dictType/page', {params}),
}
