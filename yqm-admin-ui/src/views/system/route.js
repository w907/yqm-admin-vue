const Layout = () => import('@/layout/index.vue')

export default {
    name: 'system',
    path: '/system',
    component: Layout,
    redirect: '/system/role',
    meta: {
        title: '系统管理',
        icon: 'uil:setting',
        order: 1,
    },
    children: [
        {
            name: 'system-role',
            path: 'role',
            component: () => import('./role/index.vue'),
            meta: {
                title: '角色管理',
                icon: 'carbon:user-role',
                order: 0,
            },
        },
        {
            name: 'system-post',
            path: 'post',
            component: () => import('./post/index.vue'),
            meta: {
                title: '岗位管理',
                icon: 'fa6-solid:signs-post',
                order: 0,
            },
        },
        {
            name: 'system-dept',
            path: 'dept',
            component: () => import('./dept/index.vue'),
            meta: {
                title: '部门管理',
                icon: 'ri:door-line',
                order: 0,
            },
        },
        {
            name: 'system-user',
            path: 'user',
            component: () => import('./user/index.vue'),
            meta: {
                title: '用户管理',
                icon: 'ant-design:user-outlined',
                order: 0,
            },
        },
        {
            name: 'system-dict-type',
            path: 'dict/type',
            component: () => import('./dict-type/index.vue'),
            meta: {
                title: '字典管理',
                icon: 'fluent-mdl2:dictionary',
                order: 0,
            },
        },
        {
            name: 'system-menu',
            path: 'menu',
            component: () => import('./menu/index.vue'),
            meta: {
                title: '菜单管理',
                icon: 'jam:menu',
                order: 0,
            },
        },
        {
            name: 'system-dict-data',
            path: 'dict/data',
            component: () => import('./dict-data/index.vue'),
            isHidden: true, // 隐藏
            meta: {
                title: '字典数据',
                icon: 'fluent-mdl2:dictionary',
                order: 0,
            },
        },
        {
            name: 'system-config',
            path: 'config',
            component: () => import('./config/index.vue'),
            meta: {
                title: '参数配置管理',
                icon: 'icon-park-outline:setting-config',
                order: 0,
            },
        },
    ],
}
