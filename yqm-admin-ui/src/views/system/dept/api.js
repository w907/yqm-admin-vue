import {request} from '@/utils'

export default {
    deptPage: (params = {}) => request.get('/dept/page', {params}),
    deptList: (params = {}) => request.get('/dept/list', {params}),
    deptGetById: (id) => request.get(`/dept/${id}`),
    addDept: (data) => request.post(`/dept`, data),
    updateDept: (data) => request.put(`/dept`, data),
    deleteDept: (id) => request.delete(`/dept/${id}`),
}
