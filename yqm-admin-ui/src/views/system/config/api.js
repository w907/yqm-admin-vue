import {request} from '@/utils'

export default {
    configPage: (params = {}) => request.get('/config/page', {params}),
    configGetById: (id) => request.get(`/config/${id}`),
    addConfig: (data) => request.post(`/config`, data),
    updateConfig: (data) => request.put(`/config`, data),
    deleteConfig: (id) => request.delete(`/config/${id}`),
}
