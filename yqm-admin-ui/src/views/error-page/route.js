const Layout = () => import('@/layout/index.vue')

export default {
    name: 'ErrorPage',
    path: '/error-page',
    component: Layout,
    isHidden: true, // 隐藏
    redirect: '/error-page/404',
    meta: {
        title: '错误页',
        icon: 'mdi:alert-circle-outline',
        order: 99,
    },
    children: [
        {
            name: 'ERROR-404',
            path: '404',
            isHidden: true, // 隐藏
            component: () => import('./404.vue'),
            meta: {
                title: '404',
                icon: 'tabler:error-404',
            },
        },
    ],
}
