const Layout = () => import('@/layout/index.vue')

export const basicRoutes = [
    {
        name: '404',
        path: '/404',
        component: () => import('@/views/error-page/404.vue'),
        isHidden: true,
    },

    {
        name: 'Login',
        path: '/login',
        component: () => import('@/views/login/index.vue'),
        isHidden: true,
        meta: {
            title: '登录页',
        },
    },

    // {
    //     name: 'ExternalLink',
    //     path: '/external-link',
    //     component: Layout,
    //     meta: {
    //         title: '外部链接',
    //         icon: 'mdi:link-variant',
    //         order: 97,
    //     },
    //     children: [
    //         // {
    //         //     name: 'LinkGithubSrc',
    //         //     path: 'https://gitee.com/w907/yqm-admin-vue',
    //         //     meta: {
    //         //         title: '源码 - github',
    //         //         icon: 'mdi:github',
    //         //     },
    //         // },
    //         {
    //             name: 'LinkGiteeSrc',
    //             path: 'https://gitee.com/w907/yqm-admin-vue',
    //             meta: {
    //                 title: '源码 - gitee',
    //                 icon: 'simple-icons:gitee',
    //             },
    //         },
    //     ],
    // },
]

export const NOT_FOUND_ROUTE = {
    name: 'NotFound',
    path: '/:pathMatch(.*)*',
    redirect: '/404',
    isHidden: true,
}

export const EMPTY_ROUTE = {
    name: 'Empty',
    path: '/:pathMatch(.*)*',
    component: null,
}

const modules = import.meta.glob('@/views/**/route.js', {eager: true})
const asyncRoutes = []
Object.keys(modules).forEach((key) => {
    asyncRoutes.push(modules[key].default)
})

export {asyncRoutes}
