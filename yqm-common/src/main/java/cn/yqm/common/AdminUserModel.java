package cn.yqm.common;

import lombok.Data;

/**
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/7/23
 */
@Data
public class AdminUserModel {

    private String id;
    private String account;


}
