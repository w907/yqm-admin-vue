package cn.yqm.common;

import com.alibaba.fastjson2.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * 响应
 *
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/7/23
 */
@Data
@AllArgsConstructor
@Builder
public class ResultResponse {

    /**
     * 响应代码
     */
    private int code;

    /**
     * 响应消息
     */
    private String message;

    /**
     * 响应结果
     */
    private Object result;

    /**
     * 请求环境
     */
    private String env;

    public ResultResponse() {
    }

    /**
     * 成功
     *
     * @return
     */
    public static ResultResponse success() {
        return success(null);
    }

    /**
     * 成功
     *
     * @param data
     * @return
     */
    public static ResultResponse success(String message, Object data) {
        ResultResponse rb = new ResultResponse();
        rb.setCode(1);
        rb.setMessage(message);
        rb.setResult(data);
        return rb;
    }

    /**
     * 成功
     *
     * @param message
     * @return
     */
    public static ResultResponse success(String message) {
        ResultResponse rb = new ResultResponse();
        rb.setCode(1);
        rb.setMessage(message);
        rb.setResult(null);
        return rb;
    }

    /**
     * 成功
     *
     * @param data
     * @return
     */
    public static ResultResponse success(Object data) {
        ResultResponse rb = new ResultResponse();
        rb.setCode(1);
        rb.setMessage("操作成功");
        rb.setResult(data);
        return rb;
    }

    /**
     * 成功
     *
     * @param data
     * @return
     */
    public static ResultResponse success(Object data, String env) {
        ResultResponse rb = success(data);
        rb.setEnv(env);
        return rb;
    }


    /**
     * 失败
     */
    public static ResultResponse error(int code, String message) {
        ResultResponse rb = new ResultResponse();
        rb.setCode(code);
        rb.setMessage(message);
        rb.setResult(null);
        return rb;
    }

    /**
     * 失败
     */
    public static ResultResponse error(String message) {
        ResultResponse rb = new ResultResponse();
        rb.setCode(-1);
        rb.setMessage(message);
        rb.setResult(null);
        return rb;
    }

    /**
     * 失败
     */
    public static ResultResponse error(String message, String env) {
        ResultResponse rb = error(message);
        rb.setEnv(env);
        return rb;
    }

    @Override
    public String toString() {
        return JSONObject.toJSONString(this);
    }


}
