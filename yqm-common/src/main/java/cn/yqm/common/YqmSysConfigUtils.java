package cn.yqm.common;

import cn.yqm.common.entity.YqmSysConfig;
import cn.yqm.common.req.YqmSysConfigReq;
import cn.yqm.common.service.IYqmSysConfigService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

/**
 * 获取配置信息
 *
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2024/5/10
 */
@Component
public class YqmSysConfigUtils {

    public static String getDbYqmSysConfig(String configKey) {
        IYqmSysConfigService iYqmSysConfigService = YqmSpringUtils.getBean(IYqmSysConfigService.class);
        YqmSysConfig sysConfig = iYqmSysConfigService.getOne(iYqmSysConfigService.getQuery(YqmSysConfigReq.builder().configKey(configKey).build()));
        String configValue = sysConfig.getConfigValue();
        if (StringUtils.isNotBlank(configValue)) {
            RedisTemplate redisTemplate = YqmSpringUtils.getBean(StringRedisTemplate.class);
            redisTemplate.opsForValue().set(configKey, configValue);
        }
        return configValue;
    }

    public static String getCacheYqmSysConfig(String configKey) {
        RedisTemplate redisTemplate = YqmSpringUtils.getBean(StringRedisTemplate.class);
        return (String) redisTemplate.opsForValue().get(configKey);
    }

    public static String getYqmSysConfig(String configKey) {
        String configValue = getCacheYqmSysConfig(configKey);
        if (StringUtils.isNotBlank(configValue)) {
            return configValue;
        }
        return getDbYqmSysConfig(configKey);
    }

}
