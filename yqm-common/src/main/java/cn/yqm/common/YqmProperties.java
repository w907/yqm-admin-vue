package cn.yqm.common;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/8/4
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "yqm")
public class YqmProperties {

    public final String userDir = System.getProperty("user.dir");

    /**
     * 域名
     */
    private String domain;

    /**
     * 加密的 key
     */
    private String cipherKey;

    private YqmPropertiesFile file = new YqmPropertiesFile();

    @Data
    public class YqmPropertiesFile {
        /**
         * 上传路径
         */
        private String path;

        /**
         * 存储类型
         */
        private String storeType;

        /**
         * 服务器文件路径
         */
        private String servePath;
    }

    public String getFileUploadPath() {
        return userDir + java.io.File.separatorChar + this.getFile().getPath();
    }

    public String getOpenFileServerUrl() {
        return this.domain + "/" + this.replace_servePath();
    }

    private String replace_servePath() {
        String servePath = this.getFile().getServePath();
        return servePath.replace("**", "");
    }
}
