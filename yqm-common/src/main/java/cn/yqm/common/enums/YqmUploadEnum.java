package cn.yqm.common.enums;


/**
 * 登录枚举
 *
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/7/23
 */
public enum YqmUploadEnum {

    LOCAL("local", "本地上传", "本地上传"),

    ;

    YqmUploadEnum(String value, String name, String desc) {
        this.value = value;
        this.name = name;
        this.desc = desc;
    }

    private String value;
    private String name;
    private String desc;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
