package cn.yqm.common.enums;

import cn.yqm.common.exception.YqmException;

/**
 * 状态
 *
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/8/11
 */
public enum YqmStatusEnum {


    ENABLE("0", "正常", "启用状态"),
    DEACTIVATE("1", "停用", "停用状态"),

    ;

    YqmStatusEnum(String value, String name, String desc) {
        this.value = value;
        this.name = name;
        this.desc = desc;
    }

    private String value;
    private String name;
    private String desc;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static YqmStatusEnum getValue(String status) {
        for (YqmStatusEnum statusEnum : YqmStatusEnum.values()) {
            if (statusEnum.value.equals(status)) {
                return statusEnum;
            }
        }

        throw new YqmException("状态类型不正确！请检查");
    }

}
