package cn.yqm.common.enums;

/**
 * 菜单
 *
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/8/21
 */
public enum YqmMenuEnum {
    ;

    /**
     * 菜单类型
     */
    public enum YqmMenuTypeEnum {
        M("M", "目录", "目录"),
        C("C", "菜单", "菜单"),
        F("F", "按钮", "按钮"),

        ;

        YqmMenuTypeEnum(String value, String name, String desc) {
            this.value = value;
            this.name = name;
            this.desc = desc;
        }

        private String value;
        private String name;
        private String desc;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }
    }
}
