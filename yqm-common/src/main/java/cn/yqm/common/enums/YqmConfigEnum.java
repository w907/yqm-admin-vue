package cn.yqm.common.enums;

/**
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2024/5/10
 */
public enum YqmConfigEnum {

    SYS_USER_INIT_PASSWORD("sys.user.initPassword", "用户管理-账号初始密码", "用户管理-账号初始密码");


    YqmConfigEnum(String value, String name, String desc) {
        this.value = value;
        this.name = name;
        this.desc = desc;
    }

    private String value;
    private String name;
    private String desc;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
