package cn.yqm.common.service.impl;


import cn.yqm.common.entity.YqmSysConfig;
import cn.yqm.common.mapper.YqmSysConfigMapper;
import cn.yqm.common.req.YqmSysConfigReq;
import cn.yqm.common.service.IYqmSysConfigService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 参数配置表 服务实现类
 * </p>
 *
 * @author weiximei
 * @since 2023-08-20
 */
@Service
public class YqmSysConfigServiceImpl extends ServiceImpl<YqmSysConfigMapper, YqmSysConfig> implements IYqmSysConfigService {

    @Override
    public QueryWrapper<YqmSysConfig> getQuery(YqmSysConfigReq req) {
        QueryWrapper<YqmSysConfig> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(req.getConfigName())) {
            queryWrapper.eq("config_name", req.getConfigName());
        }
        if (StringUtils.isNotBlank(req.getConfigNameLike())) {
            queryWrapper.like("config_name", req.getConfigNameLike());
        }
        if (StringUtils.isNotBlank(req.getConfigKey())) {
            queryWrapper.eq("config_key", req.getConfigKey());
        }
        if (StringUtils.isNotBlank(req.getConfigKeyLike())) {
            queryWrapper.like("config_key", req.getConfigKeyLike());
        }
        queryWrapper.orderByDesc("update_time");
        return queryWrapper;
    }
}
