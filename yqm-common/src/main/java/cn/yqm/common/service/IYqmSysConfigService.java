package cn.yqm.common.service;

import cn.yqm.common.entity.YqmSysConfig;
import cn.yqm.common.req.YqmSysConfigReq;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 参数配置表 服务类
 * </p>
 *
 * @author weiximei
 * @since 2023-08-20
 */
public interface IYqmSysConfigService extends IService<YqmSysConfig> {

    /**
     * 查询条件
     *
     * @param req
     * @return
     */
    QueryWrapper<YqmSysConfig> getQuery(YqmSysConfigReq req);

}
