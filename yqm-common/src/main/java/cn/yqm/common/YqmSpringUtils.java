package cn.yqm.common;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/7/23
 */
@Component
public class YqmSpringUtils implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    // @Autowired
    @Override
    public void setApplicationContext(ApplicationContext context) throws BeansException {
        applicationContext = context;
    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public static Object getBean(String name) {
        return getApplicationContext().getBean(name);
    }

    public static <T> T getBean(Class<T> clazz) {
        return getApplicationContext().getBean(clazz);
    }

    public static <T> T getBean(String name, Class<T> clazz) {
        return getApplicationContext().getBean(name, clazz);
    }

    public static <T> List<T> getBeanList(Class<T> clazz) {
        Map<String, T> maps = getApplicationContext().getBeansOfType(clazz);
        return maps.values().stream().toList();
    }


}
