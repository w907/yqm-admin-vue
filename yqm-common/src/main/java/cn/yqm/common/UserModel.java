package cn.yqm.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/7/23
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserModel {

    private String id;
    private String account;


}
