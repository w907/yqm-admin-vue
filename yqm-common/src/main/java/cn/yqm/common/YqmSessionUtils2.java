package cn.yqm.common;

import cn.dev33.satoken.session.SaSession;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.yqm.common.exception.YqmException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Objects;

/**
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/7/23
 */
@Slf4j
public class YqmSessionUtils2 {

    /**
     * 纠正一下：ServletRequestAttributes 这个不能作为常量获取，因为每次request请求。
     * 都会建立新的RequestAttributes，如果使用static  final 修饰，会导致无法获取到新的 RequestAttributes。
     * 从而导致无法获取到session
     * <p>
     * private static final ServletRequestAttributes SERVLET_REQUEST_ATTRS;
     * static {
     * SERVLET_REQUEST_ATTRS = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
     * }
     */

    private static final String SESSION_USER_KEY = "userInfo";
    private static final String SESSION_IMG_CODE_KEY = "imgCode";

    /**
     * 获取最新的 ServletRequestAttributes
     *
     * @return
     */
    private static ServletRequestAttributes getServletRequestAttrs() {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        return requestAttributes;
    }

    /**
     * HttpServletRequest 获取
     *
     * @return HttpServletRequest
     */
    public static HttpServletRequest getRequest() {
        return getServletRequestAttrs().getRequest();
    }

    /**
     * @return HttpSession
     */
    public static HttpSession getSession() {
        return getServletRequestAttrs().getRequest().getSession();
    }

    public static HttpServletResponse getResponse() {
        return getServletRequestAttrs().getResponse();
    }

    /**
     * SaSession
     *
     * @return
     */
    public static SaSession getSaSession() {
        return StpUtil.getSession();
    }


    /**
     * 从Session中获取用户信息
     *
     * @return user
     */
    public static UserModel getSessionUser() {
        UserModel userModel = (UserModel) getSaSession().get(SESSION_USER_KEY);
        if (Objects.isNull(userModel)) {
            throw new YqmException(-2, "请登录");
        }
        return userModel;
    }

    public static void setSessionUser(UserModel user) {
        getSaSession().set(SESSION_USER_KEY, user);
    }

    public static void removeSessionUser() {
        getSaSession().delete(SESSION_USER_KEY);
    }

    public static String getBaseUrl() {
        HttpServletRequest request = getRequest();
        return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";
    }

    /**
     * 从Session中获取 图形验证码
     *
     * @return user
     */
    public static UserModel getSessionImgCode() {
        return (UserModel) getSaSession().get(SESSION_IMG_CODE_KEY);
    }

    public static void setSessionImgCode(String code) {
        getSaSession().set(SESSION_IMG_CODE_KEY, code);
    }


    public static String uuid() {
        return StrUtil.removeAll(IdUtil.getSnowflake().nextIdStr(), "-").toUpperCase();
    }


}
