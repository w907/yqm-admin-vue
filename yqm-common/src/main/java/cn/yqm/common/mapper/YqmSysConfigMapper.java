package cn.yqm.common.mapper;

import cn.yqm.common.entity.YqmSysConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 参数配置表 Mapper 接口
 * </p>
 *
 * @author weiximei
 * @since 2023-08-20
 */
public interface YqmSysConfigMapper extends BaseMapper<YqmSysConfig> {

}
