package cn.yqm.common.config;

import cn.yqm.common.YqmSessionUtils2;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/7/23
 */

@Component
@Slf4j
public class YqmMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        this.setFieldValByName("createBy", YqmSessionUtils2.getSessionUser().getId(), metaObject);
        this.setFieldValByName("updateBy", YqmSessionUtils2.getSessionUser().getId(), metaObject);
        this.setFieldValByName("createTime", LocalDateTime.now(), metaObject);
        this.setFieldValByName("updateTime", LocalDateTime.now(), metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName("updateBy", YqmSessionUtils2.getSessionUser().getId(), metaObject);
        this.setFieldValByName("updateTime", LocalDateTime.now(), metaObject);
    }

}
