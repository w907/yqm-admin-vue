package cn.yqm.common.config;

import cn.dev33.satoken.exception.NotLoginException;
import cn.yqm.common.ResultResponse;
import cn.yqm.common.exception.YqmException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/7/23
 */
@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 处理自定义的业务异常
     *
     * @param e
     * @return
     */
    @ExceptionHandler(value = YqmException.class)
    @ResponseBody
    public ResultResponse bizExceptionHandler(YqmException e) {
        log.error("发生业务异常！原因是：{}", e.getErrorMsg());
        return ResultResponse.error(e.getErrorCode(), e.getErrorMsg());
    }

    /**
     * 处理其他异常
     *
     * @param e
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResultResponse exceptionHandler(Exception e) {
        log.error("未知异常！原因是:", e);
        return ResultResponse.error("服务器繁忙");
    }

    /**
     * token无效处理
     *
     * @param e
     * @return
     */
    @ExceptionHandler(value = NotLoginException.class)
    @ResponseBody
    public ResultResponse notLoginExceptionHandler(Exception e) {
        log.error("token 失效！原因是:", e);
        return ResultResponse.error(-2, "请登录");
    }


}
