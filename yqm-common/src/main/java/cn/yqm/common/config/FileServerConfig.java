package cn.yqm.common.config;

import cn.yqm.common.YqmProperties;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/8/20
 */

@AllArgsConstructor
@Configuration
public class FileServerConfig implements WebMvcConfigurer {

    private final YqmProperties yqmProperties;

    /**
     * 拦截处理请求信息
     * 添加文件真实地址
     *
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(yqmProperties.getFile().getServePath())
                .addResourceLocations("file:" + yqmProperties.getFileUploadPath() + "/");
    }

}
