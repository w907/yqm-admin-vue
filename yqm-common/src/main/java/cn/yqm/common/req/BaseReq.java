package cn.yqm.common.req;

import lombok.Data;

/**
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/8/6
 */

@Data
public class BaseReq {

    private Integer page = 1;
    private Integer pageSize = 10;

}
