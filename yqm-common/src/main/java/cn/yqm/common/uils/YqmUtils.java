package cn.yqm.common.uils;

import cn.yqm.common.exception.YqmException;
import com.alibaba.fastjson2.JSONObject;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.Objects;

/**
 * @author weiximei
 * @domain www.yqmshop.cn
 * @since 2023/8/8
 */

@Slf4j
public class YqmUtils {


    /**
     * 判断每个字段是否为 null
     *
     * @param m
     * @return true 某个字段不为 null false 某个字段为 null
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    public static Boolean checkObjectIsNotNull(Object m) {
        Boolean result = false;
        try {
            if (m != null) {
                JSONObject jsonObject = JSONObject.from(m);
                for (Map.Entry<String, Object> e : jsonObject.entrySet()) {
                    if (Objects.nonNull(e.getValue())) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new YqmException("参数错误！请联系研发人员检查");
        }

        return result;
    }

}
